#ifndef OS_IO_H_INCLUDED
#define OS_IO_H_INCLUDED

/*
    This module deals with the problem of thread-safe access to text output for different threads.

    The macro os_printf() is exported, which wraps printf() access.
*/

#include "board_basic_api.h"
#include "ethernet_consoles.h"
#include "circular_buffer.h"
#include "ff_stdio.h"

#ifndef NULL
#define NULL                            ((void *)0)
#endif

/* A maximum number of files on an SD card to scan */
#define MAX_FILES_SCANNED               255

#define OS_IO_STRING_BUFFER_LENGTH      81

extern volatile SemaphoreHandle_t printf_mutex;
extern volatile uint8_t string_buffer[];

uint8_t init_os_io_engine(void);

#define os_printf(...)                  do {\
                                                if (xSemaphoreTake(printf_mutex,portMAX_DELAY)==pdTRUE)\
                                                {\
                                                    printf(__VA_ARGS__);\
                                                    xSemaphoreGive(printf_mutex);\
                                                }\
                                            } while (0)
/* Circular buffer access functions are thread-aware, so mutex is used only to safely access string_buffer. */
#define eth_printf(dest,...)            do {\
                                                if (xSemaphoreTake(printf_mutex,portMAX_DELAY) == pdTRUE)\
                                                {\
                                                    snprintf((char *)string_buffer,OS_IO_STRING_BUFFER_LENGTH,__VA_ARGS__);\
                                                    circular_buffer_store_string((circular_buffer_descriptor_t *)&(dest),(uint8_t *)string_buffer);\
                                                    xSemaphoreGive(printf_mutex);\
                                                }\
                                            } while (0)

char* eth_gets(volatile circular_buffer_descriptor_t *src,char *str);

/*
    Scans the SD card for the files in a root directory, ignoring other directories.
    The entries in file_list array of strings are dynamically allocated, so they must be freed by free_file_list().
    The function returns the number of the files found.
*/
uint16_t scan_files_on_SD_card(uint8_t **file_list);
void free_file_list(uint8_t **file_list,uint16_t entries_no);

#endif /* OS_IO_H_INCLUDED */
