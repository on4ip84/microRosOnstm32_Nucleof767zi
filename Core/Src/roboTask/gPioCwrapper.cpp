/*
 * gPioCwrapper.c
 *
 *  Created on: Jan 16, 2023
 *      Author: andrey
 */
extern "C" {
#include "gPioCwrapper.h"
}
#include "sweeper_msgs/msg/light_control.h"
#include "gpio.h"
#include "tim.h"
#include "gPioClass.h"
#include "pWmClass.h"

gPioIn button(DI1_GPIO_Port, DI1_Pin);
gPioOut front_light(DO1_GPIO_Port, DO1_Pin);
gPioOut left_back_light(DO2_GPIO_Port, DO2_Pin);
gPioOut left_front_light(DO3_GPIO_Port, DO3_Pin);
gPioOut left_front_side (DO4_GPIO_Port, DO4_Pin);
gPioOut	right_back_light(DO5_GPIO_Port, DO5_Pin);
gPioOut right_front_light(DO6_GPIO_Port, DO6_Pin);
gPioOut right_front_side(DO7_GPIO_Port, DO7_Pin);

//Lin gpio control
gPioOut slipN(SLP_N_GPIO_Port,SLP_N_Pin);
gPioOut wakeN(WAKE_N_GPIO_Port,WAKE_N_Pin);
/******************************************/
// PWM out out puts
#define TIMER_CORE_CLOCK (216000000>>0)
pwmOut pwmOut1(&htim1,1000,TIMER_CORE_CLOCK);
volatile uint16_t refDt = 200;
volatile uint16_t refFrq = 2500;
volatile uint16_t startEna = 1;
uint16_t wakeState = 0;
void gPioInitC() {
	uint16_t state = 0;
	state = button.readState();
	if(startEna)pwmOut1.startPWM();
	else pwmOut1.stopPWM();

	pwmOut1.setFreq(refFrq);
	pwmOut1.setDutyPercent(refDt);
	//pwmOut1.setDutyTicks(refDt);

	slipN.Set();
	if(!wakeState)
	{

	wakeN.Set();
	wakeState=1;
	}else wakeN.Clear();
	front_light.Toggle();
	left_back_light.Toggle();

	if(__HAL_TIM_GET_FLAG(&htim3,TIM_FLAG_TRIGGER))
	{
		__HAL_TIM_CLEAR_IT(&htim3, TIM_IT_UPDATE);
		HAL_TIM_Base_Start_IT(&htim3);
	}
}

void subscription_callback3(const void *msgin) {

	// Cast received message to used type
	const sweeper_msgs__msg__LightControl *lightControl =
			(const sweeper_msgs__msg__LightControl*) msgin;
	if (lightControl->front_light != 255) {
		switch (lightControl->front_light) {
		case 0:
			front_light.Clear();
			break;
		case 1:
			front_light.Set();
			break;
		case 2:
			front_light.Toggle();
			break;
		}

	}
	if (lightControl->left_back_light != 255) {
		switch (lightControl->left_back_light) {
		case 0:
			left_back_light.Clear();
			break;
		case 1:
			left_back_light.Set();
			break;
		case 2:
			left_back_light.Toggle();
			break;
		}

	}
	if (lightControl->left_front_light != 255) {
		switch (lightControl->left_front_light) {
		case 0:
			left_front_light.Clear();
			break;
		case 1:
			left_front_light.Set();
			break;
		case 2:
			left_front_light.Toggle();
			break;
		}

	}
	if (lightControl->left_front_side != 255) {
		switch (lightControl->left_front_side) {
		case 0:
			left_front_side.Clear();
			break;
		case 1:
			left_front_side.Set();
			break;
		case 2:
			left_front_side.Toggle();
			break;
		}

	}
	if (lightControl->right_back_light != 255) {
		switch (lightControl->right_back_light) {
		case 0:
			right_back_light.Clear();
			break;
		case 1:
			right_back_light.Set();
			break;
		case 2:
			right_back_light.Toggle();
			break;
		}

	}

	if (lightControl->right_front_light == 255) {
		switch (lightControl->right_front_light) {
		case 0:
			right_front_light.Clear();
			break;
		case 1:
			right_front_light.Set();
			break;
		case 2:
			right_front_light.Toggle();
			break;
		}

	}
	if (lightControl->right_front_side != 255) {
		switch (lightControl->right_front_side) {
		case 0:
			right_front_side.Clear();
			break;
		case 1:
			right_front_side.Set();
			break;
		case 2:
			right_front_side.Toggle();
			break;
		}

	}

}
