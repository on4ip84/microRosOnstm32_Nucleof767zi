/*
 * driveTask.c
 *
 *  Created on: Dec 22, 2022
 *      Author: andrey
 */
#include "FreeRTOS.h"
#include "task.h"


#include "driveTask.h"

// make structure for device
eldDevice_st leftDrive = { };
eldDevice_st rightDrive = { };


rtp_msgs__msg__MotorCmd motorCmd[MOTORS_NUM];
rtp_msgs__msg__ChassisCmd *pChassisCmd;

rtp_msgs__msg__MotorData motorData[MOTORS_NUM];
rtp_msgs__msg__ChassisFeedback *pChassisFeedback;

static void CmdTypeHelper(rtp_msgs__msg__MotorCmd *cmd, eldDevice_st *drive);
static void CanDatacallBack(uint16_t ID, uint8_t DLC, uint8_t *data) {
#define ID_FILTER_MASK 0x1F
	uint32_t nodeId = ID & ID_FILTER_MASK; // get node id
	if (nodeId == leftDrive.devId)
		deviceRxParse(&leftDrive, ID, data);
	if (nodeId == rightDrive.devId)
		deviceRxParse(&rightDrive, ID, data);
}
void driveTaskInit(void) {
	CanApiAddCallBack(CanDatacallBack);
	eldDevInit(0x1, SYNC_ENABLED, &leftDrive);
	leftDrive.settings.maxAcceleration = MAX_MOTOR_ACCEL;
	leftDrive.settings.maxDeacceleration = MAX_MOTOR_ACCEL;
	leftDrive.settings.maxMotorSpeed = MOTOR_MAX_SPEED_RPM;
	leftDrive.settings.maxTorquePerCent = 100;
	eldDevInit(0x2, SYNC_DISABLED, &rightDrive);

}

void driveTaskProcess(void) {
	while (1) {
		deviceTimeTick(&leftDrive);
		//deviceTimeTick(&rightDrive);
		vTaskDelay(10);
		CmdTypeHelper(&motorCmd[0],&leftDrive);
	}
}

/*******************************************************************************/
void CmdTypeHelper(rtp_msgs__msg__MotorCmd *cmd, eldDevice_st *drive) {
	switch (cmd->cmd_type) {
	case rtp_msgs__msg__MotorCmd__POSE_CMD:
		drive->set_loopValue(drive,POSITION) ;
		drive->set_refValue(drive,(int32_t)cmd->angle);
		break;
		case rtp_msgs__msg__MotorCmd__RPM_CMD:
		drive->set_loopValue(drive,SPEED);
		drive->set_refValue(drive,cmd->speed);
		break;
		case rtp_msgs__msg__MotorCmd__TORQUE_CMD:
		drive->set_loopValue(drive,TORQUE);
		drive->set_refValue(drive,cmd->torque);
		break;
	default:
		break;
	}
	if (cmd->start_flag) {
		drive->set_cmdValue(drive,DRIVE_ENABLE);
	} else
		drive->set_cmdValue(drive,DRIVE_DISABLE);

	drive->set_dirValue(drive,1);
}

void driveSubsriberCallBack(const void *msgin) {
	// Cast received message to used type
	const rtp_msgs__msg__ChassisCmd *pChassisCmdlOCAL =
			(const rtp_msgs__msg__ChassisCmd*) msgin;

	//CmdTypeHelper(pChassisCmdlOCAL->motor_cmd[0], &rightDrive);

	CmdTypeHelper(&pChassisCmdlOCAL->motor_cmd.data[1], &leftDrive);
	leftDrive.set_dirValue(&leftDrive,1); // opposite direction

	CmdTypeHelper(&pChassisCmdlOCAL->motor_cmd.data[2], &rightDrive);
	rightDrive.set_dirValue(&rightDrive,1);
}

