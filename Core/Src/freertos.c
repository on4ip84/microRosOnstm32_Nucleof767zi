/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>

#include "usart.h"

#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <rcl/subscription.h>
#include <uxr/client/transport.h>
#include <rmw_microxrcedds_c/config.h>
#include <rmw_microros/rmw_microros.h>

#include <std_msgs/msg/int32.h>
#include <act_msgs/msg/actuator_state_array.h>
#include <act_msgs/msg/actuator_command.h>

/*Include Messages for chassis control*/
#include <rtp_msgs/msg/chassis_cmd.h>
#include <rtp_msgs/msg/chassis_feedback.h>
#include <sweeper_msgs/msg/cleaning_equipment_command.h>.h>
#include <sweeper_msgs/msg/light_control.h>
#include <udp_transport.h>
/*inckude transport header*/
#include "ioRead.h"
#include "canApi.h"
#include "driveTask.h"
#include "gPioCwrapper.h"
#include "webserver.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */
#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){printf("Failed status on line %d: %d. Aborting.\n",__LINE__,(int)temp_rc); vTaskDelete(NULL);}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){printf("Failed status on line %d: %d. Continuing.\n",__LINE__,(int)temp_rc);}}

void subscription_callback1(const void *msgin);
void subscription_callback2(const void *msgin);

void timer_callback(rcl_timer_t *timer, int64_t last_call_time);
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define UART
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
uint32_t defaultTaskBuffer[ 1024 ];
osStaticThreadDef_t defaultTaskControlBlock;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .cb_mem = &defaultTaskControlBlock,
  .cb_size = sizeof(defaultTaskControlBlock),
  .stack_mem = &defaultTaskBuffer[0],
  .stack_size = sizeof(defaultTaskBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};
/* Define attributed for webserver task*/
osThreadId_t webServerTaskHandle;
uint32_t webServerTaskBuffer[ 1024];
osStaticThreadDef_t webServerTaskControlBlock;
const osThreadAttr_t webServerTask_attributes = {
  .name = "webServerTask",
  .cb_mem = &webServerTaskControlBlock,
  .cb_size = sizeof(webServerTaskControlBlock),
  .stack_mem = &webServerTaskBuffer[0],
  .stack_size = sizeof(webServerTaskBuffer),
  .priority = (osPriority_t) osPriorityBelowNormal7,
};
/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
bool cubemx_transport_open(struct uxrCustomTransport *transport);
bool cubemx_transport_close(struct uxrCustomTransport *transport);
size_t cubemx_transport_write(struct uxrCustomTransport *transport,
		const uint8_t *buf, size_t len, uint8_t *err);
size_t cubemx_transport_read(struct uxrCustomTransport *transport, uint8_t *buf,
		size_t len, int timeout, uint8_t *err);

void* microros_allocate(size_t size, void *state);
void microros_deallocate(void *pointer, void *state);
void* microros_reallocate(void *pointer, size_t size, void *state);
void* microros_zero_allocate(size_t number_of_elements, size_t size_of_element,
		void *state);
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	CanApiInit();
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */

  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */

  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */


	defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);
	webServerTaskHandle = osThreadNew(webserver_freertos_task, NULL, &webServerTask_attributes);
  /* USER CODE BEGIN RTOS_THREADS */
	//initIOtimer();
	/* add threads, ... */

  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
#define ACT_MSG_ENA 1
uint16_t inMsgCount = 0;
rcl_node_t node;
rcl_publisher_t publisher;
rcl_subscription_t subscriberMotorControl;
rcl_subscription_t subscriberEquipmentControl;
rcl_subscription_t subscriberLightControl;
rclc_executor_t executor;
// Create timer object
rcl_timer_t timer;

/*Create message data objects*/
act_msgs__msg__ActuatorState actState[2];
act_msgs__msg__ActuatorStateArray *actStateArray;

sweeper_msgs__msg__CleaningEquipmentCommand cleaningEquipment;
sweeper_msgs__msg__LightControl lightControl;



#define TIMER_PERIOD_MS 5
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */

	//renesas_e2_transport_open(NULL);
	//for(;;);
	// micro-ROS configuration
#ifdef UART
  rmw_uros_set_custom_transport(
    true,
    (void *) &huart3,
	stm32_transport_open,
    cubemx_transport_close,
    cubemx_transport_write,
    cubemx_transport_read);
#else
	struct freertos_sockaddr remote_addr;
	remote_addr.sin_family = FREERTOS_AF_INET;
	remote_addr.sin_port = FreeRTOS_htons(8889);
	remote_addr.sin_addr = FreeRTOS_inet_addr("192.168.0.99");
	rmw_uros_set_custom_transport(
	false, (void*) &remote_addr, stm32_transport_open,
			stm32_transport_close, stm32_transport_write,
			stm32_transport_read);
#endif

	while (RMW_RET_OK != rmw_uros_ping_agent(1000, 1)) {
		printf("Please, start your micro-ROS Agent first\n");
	}

	rcl_allocator_t freeRTOS_allocator =
			rcutils_get_zero_initialized_allocator();
	freeRTOS_allocator.allocate = microros_allocate;
	freeRTOS_allocator.deallocate = microros_deallocate;
	freeRTOS_allocator.reallocate = microros_reallocate;
	freeRTOS_allocator.zero_allocate = microros_zero_allocate;

	if (!rcutils_set_default_allocator(&freeRTOS_allocator)) {
		printf("Error on default allocators (line %d)\n", __LINE__);
	}

	// micro-ROS app

	/*Create message data objects*/
	memset(&actState[0], 0, sizeof(actState));
	actStateArray = act_msgs__msg__ActuatorStateArray__create();
	actStateArray->act_states.data = &actState[0];
	actStateArray->act_states.capacity = 2;
	actStateArray->act_states.size = 2;

	/*Make data objects for messages*/
	memset(&motorCmd[0], 0, sizeof(motorCmd));
	pChassisCmd = rtp_msgs__msg__ChassisCmd__create();
	pChassisCmd->motor_cmd.data = &motorCmd[0];
	pChassisCmd->motor_cmd.capacity = MOTORS_NUM;
	pChassisCmd->motor_cmd.size = MOTORS_NUM;

	memset(&motorData[0], 0, sizeof(motorData));
	pChassisFeedback = rtp_msgs__msg__ChassisFeedback__create();
	pChassisFeedback->rtp_electric_motor.data = &motorData[0];
	pChassisFeedback->rtp_electric_motor.capacity = MOTORS_NUM;
	pChassisFeedback->rtp_electric_motor.size = MOTORS_NUM;

	rclc_support_t support;
	rcl_allocator_t allocator;

	allocator = rcl_get_default_allocator();

	//create init_options
	uint16_t retCode = RCL_RET_ERROR;
	while (retCode != RCL_RET_OK) {

		retCode = rclc_support_init(&support, 0, NULL, &allocator);
		if (retCode != RCL_RET_OK)
			rclc_support_fini(&support);
	};
	rcl_ret_t rc;
	// create node
	node = rcl_get_zero_initialized_node();
	rcl_node_options_t node_ops = rcl_node_get_default_options();
	node_ops.domain_id = 3;

	//rclc_node_init_default(&node, "ros2_testAPP", "", &support);
	do {
		rc = rclc_node_init_with_options(&node, "ros2_testAPP", "", &support,
				&node_ops);
	} while (rc != RCL_RET_OK);
	// create publisher
	publisher = rcl_get_zero_initialized_publisher();
	rc = rclc_publisher_init_best_effort(&publisher, &node,
#if ACT_MSG_ENA
			ROSIDL_GET_MSG_TYPE_SUPPORT(rtp_msgs, msg, ChassisFeedback),
#else
	ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg,Int32),
#endif
			"/drive/info");

	// create subscruber
	const char *topic_name = "/drive/cmd";
	// Get message type support
	rosidl_message_type_support_t *type_support = ROSIDL_GET_MSG_TYPE_SUPPORT(
			rtp_msgs, msg, ChassisCmd);

	// Initialize a reliable subscriber
	//subscriber = rcl_get_zero_initialized_subscription();
	//memset(&subscriber, 0, sizeof(subscriber));
	do {
		rc = rclc_subscription_init_default(&subscriberMotorControl, &node,
				type_support, topic_name);
	} while (rc != RCL_RET_OK);

	topic_name = "/mpn/commands/equipment";
	// Get message type support
	type_support = ROSIDL_GET_MSG_TYPE_SUPPORT(sweeper_msgs, msg,
			CleaningEquipmentCommand);
	do {
		rc = rclc_subscription_init_default(&subscriberEquipmentControl, &node,
				type_support, topic_name);
	} while (rc != RCL_RET_OK);

	topic_name = "/mpn/commands/light";
	// Get message type support
	type_support = ROSIDL_GET_MSG_TYPE_SUPPORT(sweeper_msgs, msg, LightControl);
	do {
		rc = rclc_subscription_init_default(&subscriberLightControl, &node,
				type_support, topic_name);
	} while (rc != RCL_RET_OK);
	// Initialize timer
	const unsigned int timer_period = RCL_MS_TO_NS(TIMER_PERIOD_MS);
	rc = rclc_timer_init_default(&timer, &support, timer_period,
			timer_callback);

	// Add subscription to the executor
	executor = rclc_executor_get_zero_initialized_executor();
	unsigned int num_handles = 1 + 3;	//1 timer 3 subscriber
	rclc_executor_init(&executor, &support.context, num_handles, &allocator);
	// Add to the executor
	rclc_executor_add_subscription(&executor, &subscriberMotorControl,
			pChassisCmd, &subscription_callback1, ON_NEW_DATA);

	rclc_executor_add_subscription(&executor, &subscriberEquipmentControl,
			&cleaningEquipment, &subscription_callback2, ON_NEW_DATA);

	rclc_executor_add_subscription(&executor, &subscriberLightControl,
			&lightControl, &subscription_callback3, ON_NEW_DATA);
	// Add to the executor
	rc = rclc_executor_add_timer(&executor, &timer);

	// Spin executor to receive messages
	rclc_executor_spin(&executor);

	/*Delete init task */
	vTaskDelete(NULL);
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
// Implementation example:
void subscription_callback1(const void *msgin) {

	driveSubsriberCallBack(msgin);

	HAL_GPIO_TogglePin(DO10_GPIO_Port, DO10_Pin);
	inMsgCount = 1;
}
void subscription_callback2(const void *msgin) {
	// Cast received message to used type
	const rtp_msgs__msg__ChassisCmd *pChassisCmdlOCAL =
			(const rtp_msgs__msg__ChassisCmd*) msgin;
	HAL_GPIO_TogglePin(DO10_GPIO_Port, DO10_Pin);

}


struct timespec localTime;
void timer_callback(rcl_timer_t *timer, int64_t last_call_time) {
	static uint32_t data = 0;
	//HAL_GPIO_TogglePin(DO10_GPIO_Port, DO10_Pin);
	clock_gettime(0, &localTime);

	static uint64_t timerMs = 0;
	if (timer != NULL) {
		/*Blink io8*/
		struct rcl_timer_impl_t *pimpl = timer->impl;
		timerMs += TIMER_PERIOD_MS;
		if (!(timerMs % 500))
			IO.DIO.DO.DO_st.DO8 = ~IO.DIO.DO.DO_st.DO8;

		// set time
		actStateArray->header.stamp.nanosec = localTime.tv_nsec;
		actStateArray->header.stamp.sec = localTime.tv_sec;

		// set data
		actState[0].position = IO.DIO.DI.DI_state;
		actState[1].position = IO.DIO.DI.DI_state;

		data++;
		rcl_ret_t ret = RMW_RET_OK;
		if (inMsgCount) {
			for (size_t i = 0; i < 3; i++) {
				pChassisFeedback->rtp_electric_motor.data[i].speed =
						pChassisCmd->motor_cmd.data[i].speed;
				pChassisFeedback->rtp_electric_motor.data[i].angle =
						pChassisCmd->motor_cmd.data[i].angle;
				pChassisFeedback->rtp_electric_motor.data[i].torque =
						pChassisCmd->motor_cmd.data[i].torque;

			}
			inMsgCount = 0;
			//pChassisFeedback->header.stamp.nanosec = localTime.tv_nsec;
			//pChassisFeedback->header.stamp.sec = localTime.tv_sec;
			pChassisFeedback->header.stamp.nanosec =
					pChassisCmd->header.stamp.nanosec;
			pChassisFeedback->header.stamp.sec = pChassisCmd->header.stamp.sec;
			ret = rcl_publish(&publisher, pChassisFeedback, NULL);
		}

		inMsgCount = 0;

		if (ret != RCL_RET_OK) {
			printf("Error publishing (line %d)\n", __LINE__);
		}
	}
	//HAL_GPIO_TogglePin(DO10_GPIO_Port, DO10_Pin);
}

/* USER CODE END Application */

