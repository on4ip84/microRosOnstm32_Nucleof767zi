/*
 * ioRead.c
 *
 *  Created on: Nov 18, 2022
 *      Author: andrey
 */
#include <gPioCwrapper.h>
#include "adc.h"
#include "gpio.h"
#include "ioRead.h"
#include "timers.h"
#include <string.h>

#include "roboTask.h"
#include "driveTask.h"
IO_st IO;

uint16_t adc1DataBuffer[8];
uint16_t adc2DataBuffer[8];
uint16_t adc3DataBuffer[8];

TimerHandle_t xTimerIO;
#define TIMER_TICK_MS 10

void vTimerCallback( TimerHandle_t xTimer );


void initIOtimer(void)
{
	memset(&IO,0,sizeof(IO));
	xTimerIO = xTimerCreate("DIO timer", TIMER_TICK_MS, pdTRUE, (void*)0 , vTimerCallback);
    if( xTimerStart( xTimerIO, 0 ) == pdPASS )
    {
        /* The timer set into the Active
        state. */
    	  HAL_ADC_Start_DMA(&hadc3, &adc3DataBuffer[0], 5);

    }else IO.error = 2;
    roboTaskInit();
    driveTaskInit();

}


void vTimerCallback( TimerHandle_t xTimer )
{
		gPioInitC();
//		//HAL_GPIO_TogglePin(DO10_GPIO_Port, DO10_Pin);
//		IO.DIO.DI.DI_st.DI1 = HAL_GPIO_ReadPin(DI1_GPIO_Port, DI1_Pin);
//		IO.DIO.DI.DI_st.DI2 = HAL_GPIO_ReadPin(DI2_GPIO_Port, DI2_Pin);
//		IO.DIO.DI.DI_st.DI3 = HAL_GPIO_ReadPin(DI3_GPIO_Port, DI3_Pin);
//		IO.DIO.DI.DI_st.DI4 = HAL_GPIO_ReadPin(DI4_GPIO_Port, DI4_Pin);
//		IO.DIO.DI.DI_st.DI5 = HAL_GPIO_ReadPin(DI5_GPIO_Port, DI5_Pin);
//		IO.DIO.DI.DI_st.DI6 = HAL_GPIO_ReadPin(DI6_GPIO_Port, DI6_Pin);
//		IO.DIO.DI.DI_st.DI7 = HAL_GPIO_ReadPin(DI7_GPIO_Port, DI7_Pin);
//		IO.DIO.DI.DI_st.DI8 = HAL_GPIO_ReadPin(DI8_GPIO_Port, DI8_Pin);
//
//			/*Prosecc OUT pins*/
//		if(IO.DIO.DO.DO_st.DO1) HAL_GPIO_WritePin(DO1_GPIO_Port, DO1_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO1_GPIO_Port, DO1_Pin, GPIO_PIN_RESET);
//
//		if(IO.DIO.DO.DO_st.DO2) HAL_GPIO_WritePin(DO2_GPIO_Port, DO2_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO2_GPIO_Port, DO2_Pin, GPIO_PIN_RESET);
//
//		if(IO.DIO.DO.DO_st.DO3) HAL_GPIO_WritePin(DO3_GPIO_Port, DO3_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO3_GPIO_Port, DO3_Pin, GPIO_PIN_RESET);
//
//		if(IO.DIO.DO.DO_st.DO4) HAL_GPIO_WritePin(DO4_GPIO_Port, DO4_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO4_GPIO_Port, DO4_Pin, GPIO_PIN_RESET);
//
//		if(IO.DIO.DO.DO_st.DO5) HAL_GPIO_WritePin(DO5_GPIO_Port, DO5_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO5_GPIO_Port, DO5_Pin, GPIO_PIN_RESET);
//
//		if(IO.DIO.DO.DO_st.DO6) HAL_GPIO_WritePin(DO6_GPIO_Port, DO6_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO6_GPIO_Port, DO6_Pin, GPIO_PIN_RESET);
//
//		if(IO.DIO.DO.DO_st.DO7) HAL_GPIO_WritePin(DO7_GPIO_Port, DO7_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO7_GPIO_Port, DO7_Pin, GPIO_PIN_RESET);
//
//		if(IO.DIO.DO.DO_st.DO8) HAL_GPIO_WritePin(DO8_GPIO_Port, DO8_Pin, GPIO_PIN_SET);
//		else HAL_GPIO_WritePin(DO8_GPIO_Port, DO8_Pin, GPIO_PIN_RESET);

		/*read analog inputs*/
		IO.AI.AD1 = adc1DataBuffer[0];
		IO.AI.AD2 = adc2DataBuffer[0];
		IO.AI.AD3 = adc3DataBuffer[0];

}





