/*
 * pWmClass.h
 *
 *  Created on: Jan 18, 2023
 *      Author: andrey
 */

#ifndef CORE_SRC_ROBOTASK_PWMCLASS_H_
#define CORE_SRC_ROBOTASK_PWMCLASS_H_
#include <stdint.h>
#include <stdbool.h>
#include <stm32f7xx_hal.h>
class pwmOut {
public:
	pwmOut(TIM_HandleTypeDef *htim, uint32_t baseFreq,uint32_t baseTimerFreq) {
		_htim = htim;
		_baseTimerFreq=baseTimerFreq;
		setFreq(baseFreq);
	}
	void stopPWM() {
		_htim->Instance->CR1 &= ~(TIM_CR1_CEN);	//disable main counter
		_htim->Instance->CCER &= ~(TIM_CCER_CC1E);	//disable compare counter
	}
	;	//disable timer
	void startPWM() {
		_htim->Instance->CR1 |= (TIM_CR1_CEN);	//enable main counter
		_htim->Instance->CCER |= (TIM_CCER_CC1E);	//enable compare counter
	}
	; //enable base timer
	void setFreq(uint32_t frq) {
		maxTicks = _baseTimerFreq / frq;
		_htim->Instance->PSC = maxTicks/65536;
		//calc new real freq
		uint32_t bufFreq = _baseTimerFreq/(_htim->Instance->PSC +1 );
		maxTicks = bufFreq / frq;
		_htim->Instance->ARR = maxTicks;
		maxTicks = _htim->Instance->ARR;
	}
	;
	uint32_t getMaxTicks(void) {
		return (maxTicks);
	}
	;
	/*set out put duty in percent*/
	void setDutyPercent(uint16_t duty) {
		uint16_t localDuty = (duty < 1000) ? duty : 1000;
		setDutyTicks((maxTicks * localDuty ) / 1000);

	}
	;
	/*set out put duty in base Ticks*/
	void setDutyTicks(uint16_t dutyTicks) {
		_htim->Instance->CCR1 = dutyTicks;
	}
	;
private:
	TIM_HandleTypeDef *_htim;
	uint32_t _baseFreq;
	uint32_t maxTicks;
	uint32_t _baseTimerFreq;
};

#endif /* CORE_SRC_ROBOTASK_PWMCLASS_H_ */
