/*
 * linApi.c
 *
 *  Created on: Jan 30, 2023
 *      Author: andrey
 */

#include <stdio.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "linApi.h"

linMessage_st linMessage;
linMessage_st linTxMessage;
uint16_t linReadNotify(linMessage_st  *msg)
{
	if(msg->ID == 0x3d)
	{
		/**Reply*/

		linTxMessage.ID = 0x2b;
		linTxMessage.size = 4;
		linTxMessage.data[0] = 0x4;
		linTxMessage.data[1] = 0x3;
		linTxMessage.data[2] = 0x2;
		linTxMessage.data[3] = 0x1;
		LinApiSend(&linTxMessage);
	}
	return 1;
}

uint8_t LinApiSend(linMessage_st  *msg)
{
	linSend(msg);
}
