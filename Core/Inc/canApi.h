/*
 * robotTask.h
 *
 *  Created on: Dec 14, 2022
 *      Author: andrey
 */

#ifndef CORE_INC_CANAPI_H_
#define CORE_INC_CANAPI_H_

#include "can.h"

uint16_t canReadNotify(CAN_RxHeaderTypeDef   *RxHeader,uint8_t *RxData);

uint16_t canWriteNotify(CAN_TxHeaderTypeDef   *TxHeader,uint8_t *TxData);

void CanApiInit(void);
void CanApiReadProcess(void);
uint8_t CanApiAddCallBack(void *pCallBackFunc);
uint8_t CanApiSend(uint16_t ID,uint8_t DLC,uint8_t *data);

#endif /* CORE_INC_CANAPI_H_ */
