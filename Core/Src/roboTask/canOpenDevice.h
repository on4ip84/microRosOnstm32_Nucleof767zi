/*
 * canOpenDevice.h
 *
 *  Created on: Dec 23, 2022
 *      Author: andrey
 */

#ifndef CORE_SRC_ROBOTASK_CANOPENDEVICE_H_
#define CORE_SRC_ROBOTASK_CANOPENDEVICE_H_

#include "stdint.h"
#include "canApi.h"
/*CANopen protocol data transfer definitions*/
#define SDO_W 0x600
#define SDO_R 0x580
#define wr4b 0x23
#define wr2b 0x2b
#define wr1b 0x2f
#define rd 0x40
#define PDO1_RX 0x180
#define PDO2_RX 0x280
#define PDO3_RX 0x380
#define PDO4_RX 0x480
#define PDO1_TX 0x200
#define PDO2_TX 0x300
#define PDO3_TX 0x400
#define PDO4_TX 0x500
#define SYNC_ENABLED 0x1
#define SYNC_DISABLED 0x0
void sendNMT(uint8_t devId, uint8_t cmd);
void sendSYNC(uint8_t syncEna) ;
void sendPDO(uint8_t devId,uint16_t pdo,uint8_t *data);
void sendSDO(uint8_t devId,uint8_t cmd,uint16_t index,uint8_t subIndex,uint32_t data);

#endif /* CORE_SRC_ROBOTASK_CANOPENDEVICE_H_ */
