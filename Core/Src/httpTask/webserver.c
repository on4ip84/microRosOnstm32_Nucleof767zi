#include "webserver.h"

uint8_t web_server_buffer[WEB_BUFFER_LEN];
uint8_t generated_html_buffer[WEB_BUFFER_LEN];

void send_error404(Socket_t conn)
{
    os_printf("Page not found, reporting HTTP 404\r\n");
    sprintf((char *)web_server_buffer,"HTTP/1.1 404 Not Found\r\nContent-Length: %i\r\n\r\n",strlen((char *)error_404));
    FreeRTOS_send(conn,web_server_buffer,strlen((char *)web_server_buffer),0);
    FreeRTOS_send(conn,error_404,strlen((char *)error_404),0);
}

void send_error500(Socket_t conn)
{
    os_printf("Internal server error occurred, reporting HTTP 500.\r\n");
    sprintf((char *)web_server_buffer,"HTTP/1.1 500 Internal Server Error\r\nContent-Length: %i\r\n\r\n",(int)strlen((char *)error_500));
    FreeRTOS_send(conn,web_server_buffer,strlen((char *)web_server_buffer),0);
    FreeRTOS_send(conn,error_500,strlen((char *)error_500),0);
}

void connection_handler(void* params)
{
    Socket_t connection;
    int32_t bytes_received;
    int16_t generated_length;
    uint32_t k;
    uint16_t chunk_ptr,chunk_length;
    BaseType_t result;
    uint8_t method[10],url[MAX_WEBPAGE_NAME_LEN];

    connection = (Socket_t)params;

    while (1)
    {
        bytes_received = FreeRTOS_recv(connection,web_server_buffer,WEB_BUFFER_LEN,0);

        if (bytes_received>0)
        {
            web_server_buffer[bytes_received]=0;
            sscanf((char *)web_server_buffer,"%s %s",(char *)method,(char *)url);
            os_printf("%s request received for URL = %s\r\n",(char *)method,(char *)url);

            if (strcmp((char *)method,"GET")==0)
            {
                k=0;
                while ((strcmp((char *)pagelist[k].name_url,(char *)url)!=0) && (pagelist[k].webpage_data_ptr!=NULL) && (k<MAX_PAGES))
                {
                    k++;
                }

                if (k<MAX_PAGES)
                {
                    if (pagelist[k].webpage_data_ptr==NULL)
                    {
                        send_error404(connection);
                    }
                    else
                    {
                        sprintf((char *)web_server_buffer,"HTTP/1.1 200 OK\r\nContent-Length: %i\r\n\r\n",(int)pagelist[k].webpage_size);
                        FreeRTOS_send(connection,web_server_buffer,strlen((char *)web_server_buffer),0);

                        chunk_ptr=0;
                        while (chunk_ptr<pagelist[k].webpage_size)
                        {
                            chunk_length = pagelist[k].webpage_size - chunk_ptr;

                            if (chunk_length>TX_CHUNK_SIZE)
                            {
                                chunk_length=TX_CHUNK_SIZE;
                            }

                            //os_printf("Sending webpage: %i bytes in chunk of %i total.\r\n",chunk_length,pagelist[k].webpage_size);

                            result = FreeRTOS_send(connection,&(pagelist[k].webpage_data_ptr[chunk_ptr]),chunk_length,0);

                            //os_printf("send() result: %i\r\n",(int)result);

                            chunk_ptr+=TX_CHUNK_SIZE;
                        }

                        os_printf("Page served: %s\r\n",url);
                    }
                }
                else
                {
                    send_error500(connection);
                }
            }

            if (strcmp((char *)method,"POST")==0)
            {
                k=0;
                while ((strcmp((char *)page_handlers[k].name_url,(char *)url)!=0) && (page_handlers[k].handler!=NULL) && (k<MAX_PAGES))
                {
                    k++;
                }

                if (k<MAX_PAGES)
                {
                    if (page_handlers[k].handler!=NULL)
                    {
                        generated_length = page_handlers[k].handler(web_server_buffer,strlen((char *)web_server_buffer),generated_html_buffer,WEB_BUFFER_LEN);

                        if ((generated_length>0) && (generated_length<WEB_BUFFER_LEN))
                        {
                            os_printf("CGI handler generated %i bytes of data.\r\n",(int)generated_length);
                            sprintf((char *)web_server_buffer,"HTTP/1.1 200 OK\r\nContent-Length: %i\r\n\r\n",generated_length);
                            FreeRTOS_send(connection,web_server_buffer,strlen((char *)web_server_buffer),0);
                            FreeRTOS_send(connection,generated_html_buffer,strlen((char *)generated_html_buffer),0);
                            os_printf("Page served.\r\n");
                        }
                        else
                        {
                            send_error500(connection);
                            os_printf("The CGI handler has returned wrong length (%i bytes). Error 500 was sent.\r\n",(int)generated_length);
                        }
                    }
                    else
                    {
                        send_error404(connection);
                    }
                }
                else
                {
                    send_error500(connection);
                }
            }
        }

        if (bytes_received==0)
        {
            os_printf("Server connection timeout.\r\n");
        }

        if (bytes_received<0)
        {

            os_printf("Socket read error - closing connection.\r\n");

            FreeRTOS_shutdown(connection,FREERTOS_SHUT_RDWR);

            while (FreeRTOS_recv(connection,web_server_buffer,WEB_BUFFER_LEN,0)>=0)
            {
                vTaskDelay(25);
            }

            FreeRTOS_closesocket(connection);

            os_printf("Connection closed.\r\n");

            vTaskDelete(NULL);

        }
    }
}
uint16_t errcode = 0;
void webserver_freertos_task(void *params)
{
	Socket_t server_socket, connected_socket;
	TickType_t server_receive_timeout =  portMAX_DELAY;
    //TickType_t server_receive_timeout =  60000 / portTICK_PERIOD_MS;//portMAX_DELAY;
    struct freertos_sockaddr client_socket_addr, bind_address;
    socklen_t addr_len = sizeof(client_socket_addr);

    os_printf("Starting web server...\r\n");

    server_socket = FreeRTOS_socket(FREERTOS_AF_INET,FREERTOS_SOCK_STREAM,FREERTOS_IPPROTO_TCP);

    if (server_socket==FREERTOS_INVALID_SOCKET)
    {
        os_printf("Unable to create server socket.\r\n");

        vTaskDelete(NULL);
    }
    else
    {
        os_printf("Server socket created.\r\n");
    }
    FreeRTOS_setsockopt( server_socket,
                         0,
                         FREERTOS_SO_RCVTIMEO,
                         &server_receive_timeout,
                         sizeof( server_receive_timeout ) );

    FreeRTOS_setsockopt( server_socket,
                         0,
                         FREERTOS_SO_SNDTIMEO,
                         &server_receive_timeout,
                         sizeof( server_receive_timeout) );
//    if (FreeRTOS_setsockopt(server_socket,0,FREERTOS_SO_RCVTIMEO,&server_receive_timeout,sizeof(server_receive_timeout))!=0)
//    {
//        os_printf("Unable to set server socket timeout.\r\n");
//    }
//    /* Fill in the buffer and window sizes that will be used by
//    the socket. */
//#define ipconfigTCPMSS 256
//    xWinProps.lTxBufSize = 6  *ipconfigTCPMSS;
//    xWinProps.lTxWinSize = 3;
//    xWinProps.lRxBufSize = 6 * ipconfigTCPMSS;
//    xWinProps.lRxWinSize = 3;
//
//    /* Set the window and buffer sizes. */
//    FreeRTOS_setsockopt( server_socket,
//               0,
//               FREERTOS_SO_WIN_PROPERTIES,
//               ( void * ) &xWinProps,
//               sizeof( xWinProps ) );

    bind_address.sin_port = 80; //HTTP server port
    bind_address.sin_port = FreeRTOS_htons(bind_address.sin_port);

    if(FreeRTOS_bind(server_socket,&bind_address,sizeof(bind_address))) errcode =0x1;

    if(FreeRTOS_listen(server_socket,5)) errcode|=0x2;

    os_printf("Webserver: listening on port 80.\r\n");

    while (1)
    {
        connected_socket = FreeRTOS_accept(server_socket,&client_socket_addr,&addr_len);

        if (connected_socket!=FREERTOS_INVALID_SOCKET)
        {
            xTaskCreate(connection_handler,"srv_inst",configMINIMAL_STACK_SIZE*10,connected_socket,tskIDLE_PRIORITY,NULL);
        }
        else
        {
            os_printf("Error while accepting incoming connection.\r\n");
        }

        vTaskDelay(10);
    }
}
