/*
 * ioRead.h
 *
 *  Created on: Nov 18, 2022
 *      Author: andrey
 */

#ifndef CORE_INC_IOREAD_H_
#define CORE_INC_IOREAD_H_
#include "stdint.h"
#include "FreeRTOS.h"
/*Make Freertos timer instance with extern*/


typedef struct
{
	union DI_un
	{
		struct {
			uint8_t DI1:1;
			uint8_t DI2:1;
			uint8_t DI3:1;
			uint8_t DI4:1;
			uint8_t DI5:1;
			uint8_t DI6:1;
			uint8_t DI7:1;
			uint8_t DI8:1;
		}DI_st;
		uint8_t DI_state;
	}DI;
	union DO_un
	{
		struct {
			uint8_t DO1:1;
			uint8_t DO2:1;
			uint8_t DO3:1;
			uint8_t DO4:1;
			uint8_t DO5:1;
			uint8_t DO6:1;
			uint8_t DO7:1;
			uint8_t DO8:1;
		}DO_st;
		uint8_t DO_state;
	}DO;
}DIO_st;

typedef struct
{
	uint16_t AD1;
	uint16_t AD2;
	uint16_t AD3;
	uint16_t AD4;
	uint16_t AD5;
	uint16_t AD6;
	uint16_t AD7;
	uint16_t AD8;
	uint16_t AD9;
	uint16_t AD10;
	uint16_t AD11;
	uint16_t AD12;
	uint16_t AD13;
	uint16_t AD14;
	uint16_t AD15;

}AI_st;
typedef struct
{
	DIO_st DIO;
	AI_st AI;
	uint16_t error;
}IO_st;

extern IO_st IO;

void initIOtimer(void);


#endif /* CORE_INC_IOREAD_H_ */
