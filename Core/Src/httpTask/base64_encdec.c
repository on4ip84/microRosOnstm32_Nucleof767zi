#include "base64_encdec.h"

uint8_t b64_symbol_to_value(uint8_t symbol)
{
    if ((symbol >= 'A') && (symbol <= 'Z'))
    {
        return symbol - 'A';
    }

    if ((symbol >= 'a') && (symbol <= 'z'))
    {
        return 0x1A + (symbol - 'a');
    }

    if ((symbol >= '0') && (symbol <= '9'))
    {
        return 0x34 + (symbol - '0');
    }

    if (symbol == '+')
    {
        return 0x3E;
    }

    if (symbol == '/')
    {
        return 0x3F;
    }

    return 0;
}

void b64_inplace_decode(uint8_t *string)
{
    uint16_t k;
    uint16_t j;
    uint8_t buf[4];

    k=0;
    j=0;
    while (k<512)
    {
        if ((k % 4 == 0) && (k!=0))
        {
            string[j+0] = (b64_symbol_to_value(buf[0])<<2) | (b64_symbol_to_value(buf[1])>>4);
            string[j+1] = (b64_symbol_to_value(buf[1])<<4) | (b64_symbol_to_value(buf[2])>>2);
            string[j+2] = (b64_symbol_to_value(buf[2])<<6) | (b64_symbol_to_value(buf[3]));

            j+=3;
        }

        if (string[k] == 0)
        {
            break;
        }

        buf[k % 4] = string[k];

        k++;
    }

    string[j] = 0;
}

uint8_t b64_value_to_symbol(uint8_t value)
{
    if (value <= 25)
    {
        return 'A' + value;
    }

    if (value <= 51)
    {
        return 'a' + (value - 26);
    }

    if (value <= 61)
    {
        return '0' + (value - 52);
    }

    if (value == 62)
    {
        return '+';
    }

    if (value == 63)
    {
        return '/';
    }

    return '=';
}

uint8_t safe_extract_byte(uint8_t *array,uint32_t index,uint32_t array_length)
{
    if (index < array_length)
    {
        return array[index];
    }
    else
    {
        return 0;
    }
}

#include "os_io.h"

uint32_t b64_encode(uint8_t *src,uint32_t src_len,uint8_t *dst,uint8_t dst_len)
{
    uint32_t k;
    uint32_t N;
    uint32_t j;
    uint8_t padding_count;
    uint8_t b0,b1,b2;

    if ((src_len % 3) == 0)
    {
        N = src_len;
        padding_count = 0;
    }
    else
    {
        padding_count = (3 - (src_len % 3));
        N = src_len + padding_count;
    }

    j=0;

    for (k=0; k<N; k+=3)
    {
        if ((j + 4) > dst_len)
        {
            return 0;
        }

        b0 = safe_extract_byte(src,k+0,src_len);
        b1 = safe_extract_byte(src,k+1,src_len);
        b2 = safe_extract_byte(src,k+2,src_len);

        dst[j+0] = b64_value_to_symbol(b0 >> 2);
        dst[j+1] = b64_value_to_symbol(((b0 & 0x03) << 4) | (b1 >> 4));
        dst[j+2] = b64_value_to_symbol(((b1 & 0x0F) << 2) | (b2 >> 6));
        dst[j+3] = b64_value_to_symbol(b2 & 0x3F);

        j+=4;
    }

    for (k=0; k<padding_count; k++)
    {
        dst[j-k-1] = '=';
    }

    return j;
}
