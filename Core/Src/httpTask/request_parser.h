#ifndef REQUEST_PARSER_H_INCLUDED
#define REQUEST_PARSER_H_INCLUDED

#include "stdint.h"
#include "string.h"

typedef struct {
    uint8_t *key;
    uint8_t *value;
} parameter_t;

#define MAX_PARSE_LENGTH        1024
#define MAX_FIELDS              50
#define MAX_PARSED_ENRTIES      (2*MAX_FIELDS)

#ifndef NULL
#define NULL                    ((void *)0)
#endif

uint8_t parse_request(uint8_t *request,parameter_t *parsed_request,uint16_t *parsed_entries_count);
uint8_t *find_value_by_key(uint8_t *reference_key,parameter_t *parsed,uint16_t parsed_entries_count);
uint16_t inplace_urldecode(uint8_t *str_ptr);

#endif // REQUEST_PARSER_H_INCLUDED
