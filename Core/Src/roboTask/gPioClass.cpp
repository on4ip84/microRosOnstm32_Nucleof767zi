/*
 * gPioClass.cpp
 *
 *  Created on: Jan 16, 2023
 *      Author: andrey
 */

#include "gPioClass.h"
gPio::gPio(GPIO_TypeDef *_pinPort,uint16_t _pinNum)
{
	this->pinPort = _pinPort;
	this->pinNum = _pinNum;
	this->pinState = false;
}
gPio::~gPio()
{

}
