/*
 * eldCanData.h
 *
 *  Created on: Dec 23, 2022
 *      Author: andrey
 */

#ifndef CORE_SRC_ROBOTASK_ELDCANDATA_H_
#define CORE_SRC_ROBOTASK_ELDCANDATA_H_


#define MOTOR_MAX_SPEED_RPM 4000
#define ENCODER_PULSE 4000
#define MAX_MOTOR_SPEED_PULSE  (MOTOR_MAX_SPEED_RPM / 60 * ENCODER_PULSE)
#define	MAX_MOTOR_ACCEL  (MAX_MOTOR_SPEED_PULSE * 10)
#define MOTOR_SPEED_SCALE  (9.55f / 60 * ENCODER_PULSE)
#define  MOTOR_POSITION_SCALE  (4000.f / 6.28134f)
#define CMDWAITCOUNTER  20
struct ELD2_settings {
  uint32_t maxMotorSpeed;
  uint16_t maxTorquePerCent;
  uint32_t maxAcceleration;
  uint32_t maxDeacceleration;
};

struct ELD2_data_st {

  union contro_word_un {
    /* data */
    struct {
      /* data */
      uint16_t switch_on : 1;
      uint16_t enable_voltage : 1;
      uint16_t quick_stop : 1;
      uint16_t operation_enable : 1;
      uint16_t mode_specific : 3;
      uint16_t flear_fault : 1;
      uint16_t halt : 1;
    } contro_word_st;

    uint16_t control_word;
  } control;

  union status_word_un {
    /* data */
    struct {
      /* data */
      uint16_t ready_on : 1;
      uint16_t switched_on : 1;
      uint16_t operation_enabled : 1;
      uint16_t fault : 1;
      uint16_t voltage_enaled : 1;
      uint16_t quick_stop : 1;
      uint16_t switch_on_dis : 1;
      uint16_t warning : 1;
      uint16_t manufact_spec : 1;
      uint16_t remote : 1;
      uint16_t target_riched : 1;

    } status_word_st;

    uint16_t status_word;
  } status;

  uint8_t actual_mode;
  float ref_position;
  float fb_position;
  float ref_speed;
  float fb_speed;
  float ref_torque;
  float fb_torque;
  uint16_t error_code;
  uint16_t torqueLimitPercent;
};

union PDO1_TX_un {
  struct __attribute__((packed)) PDO1_TX_st {
    uint16_t command;
    int16_t torque;
    uint8_t mode;
  } PDO1_TX_st;

  uint8_t pdo_data[8];
};

union PDO1_RX_un {
  struct __attribute__((packed)) PDO1_RX_st {
    uint16_t status;
    int16_t torque;
    uint8_t mode;
  } PDO1_RX_st;

  uint8_t pdo_data[8];
};

union PDO2_TX_un {
  struct __attribute__((packed)) PDO2_TX_st {
    uint16_t command;
    int32_t position;
    uint8_t mode;
  } PDO2_TX_st;

  uint8_t pdo_data[8];
};
union PDO2_RX_un {
  struct __attribute__((packed)) PDO2_RX_st {
    uint16_t status;
    int32_t position;
    uint8_t mode;
  } PDO2_RX_st;

  uint8_t pdo_data[8];
};

union PDO3_TX_un {
  struct __attribute__((packed)) PDO3_TX_st {
    uint16_t command;
    int32_t speed;
    uint8_t mode;
  } PDO3_TX_st;

  uint8_t pdo_data[8];
};

union PDO3_RX_un {
  struct __attribute__((packed)) PDO3_RX_st {
    uint16_t status;
    int32_t speed;
    uint8_t mode;
  } PDO3_RX_st;

  uint8_t pdo_data[8];
};
union PDO4_TX_un {
  struct __attribute__((packed)) PDO4_TX_st {
    uint16_t torqueLimitPerCent;
    uint8_t res1;
    uint8_t res2;
    uint8_t res3;
    uint8_t res4;
    uint8_t res5;
    uint8_t res6;
  } PDO4_TX_st;

  uint8_t pdo_data[8];
};
union SDO_un {
  struct __attribute__((packed)) SDO_st {
    uint8_t code;
    int16_t index;
    uint8_t subindex;
    int32_t data;
  } SDO_st;

  uint8_t sdo_data[8];
};



#endif /* CORE_SRC_ROBOTASK_ELDCANDATA_H_ */
