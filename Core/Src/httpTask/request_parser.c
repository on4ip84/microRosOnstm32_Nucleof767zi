#include "request_parser.h"

int16_t find_chr_pos(uint8_t *str,uint8_t ch)
{
    uint8_t k;

    k=0;
    while ((str[k]!=0) && (k<MAX_PARSE_LENGTH))
    {
        if (str[k]==ch)
        {
            return k;
        }

        k++;
    }

    return -1;
}

uint8_t parse_request(uint8_t *request,parameter_t *parsed_request,uint16_t *parsed_entries_count)
{
    uint32_t k;
    uint16_t l_ptr;
    uint16_t e_ptr;
    uint16_t req_ptr;
    static uint8_t *browser_options[MAX_FIELDS];
    static uint8_t *request_entries[MAX_FIELDS];
    uint8_t *post_body;
    int16_t separator_pos;

    //Step 1: convert browser options to an indexed array of strings and find the beginning of the POST parameters
    k=0;
    browser_options[0] = request;
    l_ptr=1;
    while ((request[k]!=0) && (k<MAX_PARSE_LENGTH) && (l_ptr<MAX_FIELDS))
    {
        if ((request[k]=='\r') && (request[k+1]=='\n') && (request[k+2]=='\r') && (request[k+3]=='\n'))
        {
            request[k]=0;
            request[k+1]=0;
            request[k+2]=0;
            request[k+3]=0;

            post_body = &(request[k+4]);

            break;
        }

        if ((request[k]=='\r')  && (request[k+1]=='\n'))
        {
            request[k]=0;
            request[k+1]=0;
            browser_options[l_ptr] = &(request[k+2]);
            l_ptr++;
            k+=2;
        }
        else
        {
            k++;
        }
    }

    if ((k>=MAX_PARSE_LENGTH) || (l_ptr>=MAX_FIELDS))
    {
        (*parsed_entries_count)=0;
        return 0;
    }

    //Step 2: split POST request parameters to an indexed array of strings
    k=0;
    request_entries[0] = post_body;
    e_ptr=1;
    while ((post_body[k]!=0) && (k<MAX_PARSE_LENGTH) && (e_ptr<MAX_FIELDS))
    {
        if (post_body[k]=='&')
        {
            post_body[k]=0;
            request_entries[e_ptr] = &(post_body[k+1]);
            e_ptr++;
        }

        k++;
    }

    if ((k>=MAX_PARSE_LENGTH) || (e_ptr>=MAX_FIELDS))
    {
        (*parsed_entries_count)=0;
        return 0;
    }

    req_ptr=0;

    //Step 3: split browser options into indexed key/value pairs
    for (k=0; k<l_ptr; k++)
    {
        separator_pos = find_chr_pos(browser_options[k],':');

        if (separator_pos>=0)
        {
            browser_options[k][separator_pos]=0;
            browser_options[k][separator_pos+1]=0;

            parsed_request[req_ptr].key = &(browser_options[k][0]);
            parsed_request[req_ptr].value = &(browser_options[k][separator_pos+2]);

            req_ptr++;

            if (req_ptr>=MAX_PARSED_ENRTIES)
            {
                (*parsed_entries_count)=0;
                return 0;
            }
        }
    }

    //Step 4: split POST parameters into indexed key/value pairs
    for (k=0; k<e_ptr; k++)
    {
        separator_pos = find_chr_pos(request_entries[k],'=');

        if (separator_pos>=0)
        {
            request_entries[k][separator_pos]=0;

            parsed_request[req_ptr].key = &(request_entries[k][0]);
            parsed_request[req_ptr].value = &(request_entries[k][separator_pos+1]);

            req_ptr++;

            if (req_ptr>=MAX_PARSED_ENRTIES)
            {
                (*parsed_entries_count)=0;
                return 0;
            }
        }
    }

    (*parsed_entries_count)=req_ptr;

    return 1;
}

uint8_t *find_value_by_key(uint8_t *reference_key,parameter_t *parsed,uint16_t parsed_entries_count)
{
    uint16_t k;

    for (k=0; k<parsed_entries_count; k++)
    {
        if (strcmp(reference_key,parsed[k].key)==0)
        {
            return parsed[k].value;
        }
    }

    return NULL;
}

inline uint8_t hex_to_digit(uint8_t hex_char)
{
    if ((hex_char>='0') && (hex_char<='9'))
    {
        return (hex_char - '0');
    }
    else
    {
        if ((hex_char>='A') && (hex_char<='F'))
        {
            return (10 + (hex_char - 'A'));
        }
        else
        {
            return 0;
        }
    }
}

inline uint8_t hex_to_char(uint8_t *ptr)
{
    return ((hex_to_digit(ptr[0])*16) + hex_to_digit(ptr[1]));
}

uint16_t inplace_urldecode(uint8_t *str_ptr)
{
    uint16_t pos;
    uint16_t k;

    pos=0;

    while ((str_ptr[pos]!=0) && (pos<MAX_PARSE_LENGTH))
    {
        if (str_ptr[pos]=='+')
        {
            str_ptr[pos]=' ';
        }

        if (str_ptr[pos]=='%')
        {
            //Step 1: replace [%] with a corresponding character
            str_ptr[pos] = hex_to_char(&(str_ptr[pos+1]));

            //Step 2: shift string left to get rid of the code characters
            k = pos + 1;
            do{
                str_ptr[k] = str_ptr[k+2];
                str_ptr[k+1] = str_ptr[k+3];
                k+=2;
            }while ((str_ptr[k]!=0) && (k<MAX_PARSE_LENGTH));
        }

        pos++;
    }

    return pos;
}
