/*
 * driveTask.h
 *
 *  Created on: Dec 22, 2022
 *      Author: andrey
 */

#ifndef CORE_SRC_ROBOTASK_DRIVETASK_H_
#define CORE_SRC_ROBOTASK_DRIVETASK_H_

#include "canApi.h"
#include "eld2Can.h"

/*Include Messages for chassis control*/
#include <rtp_msgs/msg/chassis_cmd.h>
#include <rtp_msgs/msg/chassis_feedback.h>

#define MOTORS_NUM 4
extern rtp_msgs__msg__MotorCmd motorCmd[MOTORS_NUM];
extern rtp_msgs__msg__ChassisCmd *pChassisCmd;

extern rtp_msgs__msg__MotorData motorData[MOTORS_NUM];
extern rtp_msgs__msg__ChassisFeedback *pChassisFeedback;
void driveTaskInit(void);
void driveTaskProcess(void);

// Ros integration function
void driveSubsriberCallBack(const void *msgin);


#endif /* CORE_SRC_ROBOTASK_DRIVETASK_H_ */
