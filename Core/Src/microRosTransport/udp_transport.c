
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <udp_transport.h>

struct freertos_sockaddr *remote_addr;
Socket_t xSocket;

bool stm32_transport_open(struct uxrCustomTransport * transport){
    (void) transport;
    bool rv = false;

    xSocket = FreeRTOS_socket(FREERTOS_AF_INET, FREERTOS_SOCK_DGRAM, FREERTOS_IPPROTO_UDP);
    if (FREERTOS_INVALID_SOCKET != xSocket)
    {
        rv = true;
    }

    return rv;
}

bool stm32_transport_close(struct uxrCustomTransport * transport){
    (void) transport;
    (void) FreeRTOS_shutdown(xSocket, FREERTOS_SHUT_RDWR);
    (void) FreeRTOS_closesocket(xSocket);
    return true;
}

size_t stm32_transport_write(struct uxrCustomTransport* transport, const uint8_t * buf, size_t len, uint8_t * err){
    remote_addr = (struct freertos_sockaddr *) transport->args;
    size_t rv = 0;

    BaseType_t bytes_sent = FreeRTOS_sendto(xSocket, buf, len, 0, remote_addr, sizeof(struct freertos_sockaddr));

    if (0 <= bytes_sent)
    {
        rv = (size_t)bytes_sent;
        *err = 0;
    }
    else
    {
        *err = 1;
    }

    return rv;
}

size_t stm32_transport_read(struct uxrCustomTransport* transport, uint8_t* buf, size_t len, int timeout, uint8_t* err){
    (void) transport;
    size_t rv = 0;

    // Set read timeout
    TickType_t timeout_ticks = pdMS_TO_TICKS(timeout);
    FreeRTOS_setsockopt(xSocket, 0, FREERTOS_SO_RCVTIMEO, &timeout_ticks, 0);

    int32_t bytes_received = FreeRTOS_recvfrom(xSocket, (void*)buf, len, 0, NULL, NULL);
    if (0 <= bytes_received)
    {
        rv = (size_t)bytes_received;
        *err = 0;
    }
    else
    {
        *err = 1;
    }

    return rv;
}
