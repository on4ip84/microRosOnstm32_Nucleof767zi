Step by step example setup.
Project based on stm32F767zi mcu and using CubeIde with make file.

[[_TOC_]]

## Example downloading

First step is cloning source code into folder on local PC.
Using "git clone" commnad

## micrRos libryary compilation 

This link [micRos CubeIDE Utils](https://github.com/micro-ROS/micro_ros_stm32cubemx_utils/tree/foxy) provide full information how to compile libryary for STM32 device.

1.  Clone repo to example project folder ( where you can find Drivers MiddleWare and ect folders).
2.  Using guide information download ***FOXY*** docker image using command *docker pull microros/micro_ros_static_library_builder:foxy*.
3.  Start compilation using command 
*docker run -it --rm -v $(pwd):/project --env MICROROS_LIBRARY_FOLDER=micro_ros_stm32cubemx_utils/microros_static_library microros/micro_ros_static_library_builder:foxy*.
Settings and coustomisation for libryary may be changed in **colcon** files. 

## FreeRtos TCP libryary compilation

This link [FREERTOS TCP](https://github.com/FreeRTOS/FreeRTOS-Plus-TCP) provide source code for TCP stack under RTOS. Stack used in transport source code for microros.
1. Clone repo to example project *Middlewares/Third_Party/*.
2. Read information about FreeRtos TCP on offical site.

## Example compilation

If you didnt change IOC and generate new periphiral code, just use __build button__ and wait.
If compilation end with no errors you have to run code on target. 

## ***Example using***

After uploding binary to target, start it and check PING.
Now IP address of traget is 192.168.0.2 If ping is Ok lets move. !!! ***Attention*** working NET parameters should be set in _**FreeRtosConfig**_ header.

Read information about [micrRos](https://micro.ros.org/docs/tutorials/core/first_application_linux/) and make own workspace.
Just use docker image **ROS:FOXY** from [epoSima]( https://www.eprosima.com/index.php/component/ars/repository/micro-ros/micro-ros-foxy)

1. Run docker using __sudo docker run -it --net=host -v /dev:/dev --privileged microagent_full__
2. Build micRos agent code. Agent make bridge between MCU transport and  ROS2. Using tutorial compile agent code.
3. Go to **microros_ws** folder using __cd microros_ws__ and run  __source install/local_setup.bash__.
4. Run microRos agent 

- If you are using UART commincation run agent how ***ros2 run micro_ros_agent micro_ros_agent serial -b 115200 --dev /dev/ttyACM0*** .

- If you are using Ethernet UDP transport run agent how ***ros2 run micro_ros_agent micro_ros_agent udp4 --port 8888*** .
!!!!! Attention in project usen ROS NODE ID and you have to set it on PC side using **export ROS_DOMAIN_ID=3** 
> 
!!!!Attention ip address of local PC must be equal to IP address(mean by network and IP) in MCU example transport initialisation.
 Now it is ***192.168.0.99***. If andress of local PC different __MicroAgend__ cant hanle connection

> 
5. Now open new terminal. Get docker ID using command __docker ps__ and connect to  docker  using command __docker exec -it 65344e50e9fb bin/bash__ .
6. Run __source /opt/ros/$ROS_DISTRO/setup.bash__ .
7. Lets discover topics that active in ROS. Try __ros2 topic list__.
8. Now you have to find topic name to minitor. Example topic name is /cubemx_publisher, so run command __ros2 topic echo /cubemx_publisher__.
	
## **Test HTTP server**
In project structure added test server and page. That placed to project folder __Core/Src/httpTask__.

Webserver task should be called using FreeRtos apis.

 **Attention** Project Settings for FreeRTOS heap( file _FreeRtosConfig_) must be checked before project compilation, now heap memory size about 80kB.
