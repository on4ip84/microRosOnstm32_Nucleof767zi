#ifndef WEBSERVER_H_INCLUDED
#define WEBSERVER_H_INCLUDED

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "FreeRTOS_IP.h"
#include "FreeRTOS_TCP_IP.h"
#include "FreeRTOS_Sockets.h"
#include "stdio.h"
#include "os_io.h"

#include "request_parser.h"

extern volatile SemaphoreHandle_t printf_mutex;

#define MAX_WEBPAGE_NAME_LEN        25
#define MAX_PAGES                   10
#define WEB_BUFFER_LEN              4096
#define TX_CHUNK_SIZE               1024

typedef struct {

    const uint8_t name_url[MAX_WEBPAGE_NAME_LEN];
    const uint8_t * const webpage_data_ptr;
    const uint32_t webpage_size;

} webpage_t;

typedef uint16_t (*webpage_handler_func_t)(uint8_t *post_request,uint32_t request_length,uint8_t *html_buffer,uint32_t html_max_length);

typedef struct {
    const uint8_t name_url[MAX_WEBPAGE_NAME_LEN];
    const webpage_handler_func_t handler;
} page_handler_t;

extern const webpage_t pagelist[];
extern const page_handler_t page_handlers[];
extern const uint8_t error_404[];
extern const uint8_t error_500[];

void webserver_freertos_task(void *params);

#endif /* WEBSERVER_H_INCLUDED */
