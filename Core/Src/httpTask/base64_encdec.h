#ifndef BASE64_ENCDEC_H_INCLUDED
#define BASE64_ENCDEC_H_INCLUDED

#include "stdint.h"

void b64_inplace_decode(uint8_t *string);
uint32_t b64_encode(uint8_t *src,uint32_t src_len,uint8_t *dst,uint8_t dst_len);

#endif // BASE64_ENCDEC_H_INCLUDED
