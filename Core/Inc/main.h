/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DO8_Pin GPIO_PIN_2
#define DO8_GPIO_Port GPIOE
#define DO9_Pin GPIO_PIN_3
#define DO9_GPIO_Port GPIOE
#define DO10_Pin GPIO_PIN_4
#define DO10_GPIO_Port GPIOE
#define DO11_Pin GPIO_PIN_5
#define DO11_GPIO_Port GPIOE
#define USER_Btn_Pin GPIO_PIN_13
#define USER_Btn_GPIO_Port GPIOC
#define DO14_Pin GPIO_PIN_0
#define DO14_GPIO_Port GPIOF
#define DO12_Pin GPIO_PIN_1
#define DO12_GPIO_Port GPIOF
#define DO13_Pin GPIO_PIN_2
#define DO13_GPIO_Port GPIOF
#define DI3_Pin GPIO_PIN_6
#define DI3_GPIO_Port GPIOF
#define DI4_Pin GPIO_PIN_7
#define DI4_GPIO_Port GPIOF
#define DO15_Pin GPIO_PIN_8
#define DO15_GPIO_Port GPIOF
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOH
#define DI1_Pin GPIO_PIN_0
#define DI1_GPIO_Port GPIOC
#define RMII_MDC_Pin GPIO_PIN_1
#define RMII_MDC_GPIO_Port GPIOC
#define RMII_REF_CLK_Pin GPIO_PIN_1
#define RMII_REF_CLK_GPIO_Port GPIOA
#define RMII_MDIO_Pin GPIO_PIN_2
#define RMII_MDIO_GPIO_Port GPIOA
#define RMII_CRS_DV_Pin GPIO_PIN_7
#define RMII_CRS_DV_GPIO_Port GPIOA
#define RMII_RXD0_Pin GPIO_PIN_4
#define RMII_RXD0_GPIO_Port GPIOC
#define RMII_RXD1_Pin GPIO_PIN_5
#define RMII_RXD1_GPIO_Port GPIOC
#define LD1_Pin GPIO_PIN_0
#define LD1_GPIO_Port GPIOB
#define SLP_N_Pin GPIO_PIN_14
#define SLP_N_GPIO_Port GPIOF
#define WAKE_N_Pin GPIO_PIN_15
#define WAKE_N_GPIO_Port GPIOF
#define DI6_Pin GPIO_PIN_0
#define DI6_GPIO_Port GPIOG
#define PWM_CH1_Pin GPIO_PIN_9
#define PWM_CH1_GPIO_Port GPIOE
#define PWM_CH2_Pin GPIO_PIN_11
#define PWM_CH2_GPIO_Port GPIOE
#define RMII_TXD1_Pin GPIO_PIN_13
#define RMII_TXD1_GPIO_Port GPIOB
#define LD3_Pin GPIO_PIN_14
#define LD3_GPIO_Port GPIOB
#define STLK_RX_Pin GPIO_PIN_8
#define STLK_RX_GPIO_Port GPIOD
#define STLK_TX_Pin GPIO_PIN_9
#define STLK_TX_GPIO_Port GPIOD
#define DO4_Pin GPIO_PIN_2
#define DO4_GPIO_Port GPIOG
#define DO6_Pin GPIO_PIN_3
#define DO6_GPIO_Port GPIOG
#define USB_PowerSwitchOn_Pin GPIO_PIN_6
#define USB_PowerSwitchOn_GPIO_Port GPIOG
#define USB_OverCurrent_Pin GPIO_PIN_7
#define USB_OverCurrent_GPIO_Port GPIOG
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define DI5_Pin GPIO_PIN_15
#define DI5_GPIO_Port GPIOA
#define DI8_Pin GPIO_PIN_10
#define DI8_GPIO_Port GPIOC
#define DI2_Pin GPIO_PIN_11
#define DI2_GPIO_Port GPIOC
#define DI7_Pin GPIO_PIN_0
#define DI7_GPIO_Port GPIOD
#define DO16_Pin GPIO_PIN_1
#define DO16_GPIO_Port GPIOD
#define DI9_Pin GPIO_PIN_2
#define DI9_GPIO_Port GPIOD
#define DO2_Pin GPIO_PIN_3
#define DO2_GPIO_Port GPIOD
#define DO1_Pin GPIO_PIN_4
#define DO1_GPIO_Port GPIOD
#define DO3_Pin GPIO_PIN_5
#define DO3_GPIO_Port GPIOD
#define DO5_Pin GPIO_PIN_6
#define DO5_GPIO_Port GPIOD
#define DO7_Pin GPIO_PIN_7
#define DO7_GPIO_Port GPIOD
#define RMII_TX_EN_Pin GPIO_PIN_11
#define RMII_TX_EN_GPIO_Port GPIOG
#define RMII_TXD0_Pin GPIO_PIN_13
#define RMII_TXD0_GPIO_Port GPIOG
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define LD2_Pin GPIO_PIN_7
#define LD2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
