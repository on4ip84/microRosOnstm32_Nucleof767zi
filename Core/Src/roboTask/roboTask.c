/*
 * roboTask.c
 *
 *  Created on: Dec 22, 2022
 *      Author: andrey
 */
#include "roboTask.h"

static void CanDatacallBack(uint16_t ID,uint8_t DLC,uint8_t *data)
{
	if(ID==0x123) CanApiSend(ID, DLC, data);
}

void roboTaskInit(void)
{
	CanApiAddCallBack(CanDatacallBack);
}



