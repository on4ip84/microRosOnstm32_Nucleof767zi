/*
 * robotTask.c
 *
 *  Created on: Dec 14, 2022
 *      Author: andrey
 */
#include <stdio.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "canApi.h"
QueueHandle_t *rxQueue; //pointer to freertos queue object
QueueHandle_t *txQueue;

typedef struct {
	uint16_t ucMessageID;
	uint8_t ucDLC;
	uint8_t ucData[8];
} CanMeassage_t;

// Callback struct

#define MAX_CALLBACK 10

struct readCallBack_st {
	uint8_t callBaclNum; //Number of callback for processing in Read
	void (*pCallBaclFunc[MAX_CALLBACK])(uint16_t, uint8_t, uint8_t*); //pointer to callbacl function
} readCallBack={};
//process incoming message
uint16_t canReadNotify(CAN_RxHeaderTypeDef *RxHeader, uint8_t *RxData) {
	/* We have not woken a task at the start of the ISR. */
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	CanMeassage_t message;
	message.ucMessageID = RxHeader->StdId;
	message.ucDLC = RxHeader->DLC;
	memcpy(&message.ucData[0], RxData, message.ucDLC);
	xQueueSendFromISR(rxQueue, &message, &xHigherPriorityTaskWoken);
	return 1;
}

//process outgoing message
uint16_t canWriteNotify(CAN_TxHeaderTypeDef *TxHeader, uint8_t *TxData) {
	BaseType_t xTaskWokenByReceive = pdFALSE;

	CanMeassage_t message;
	if (xQueueReceiveFromISR(txQueue, (void*) &message,
			&xTaskWokenByReceive) == pdTRUE) {
		TxHeader->DLC = message.ucDLC;
		TxHeader->StdId = message.ucMessageID;
		memcpy(TxData, &message.ucData[0], TxHeader->DLC);
		return 1;
	}
	return 0;
}

//init robotTask

void CanApiInit(void) {
	rxQueue = xQueueCreate(10, sizeof(CanMeassage_t));

	if (rxQueue == NULL) {
		/* Queue was not created and must not be used. */
	}
	txQueue = xQueueCreate(10, sizeof(CanMeassage_t));

	if (txQueue == NULL) {
		/* Queue was not created and must not be used. */
	}

	memset(&readCallBack, 0, sizeof(readCallBack));

}
// function add Callback for processing

uint8_t CanApiAddCallBack(void *pCallBackFunc) {

	if (readCallBack.callBaclNum < MAX_CALLBACK) {
		readCallBack.pCallBaclFunc[readCallBack.callBaclNum] = pCallBackFunc;
		readCallBack.callBaclNum++;
		return (0);
	}
	return (1);
}
// process canRead Task function

void CanApiReadProcess(void) {
	static CanMeassage_t message;
	while (1) {
		if (xQueueReceive(rxQueue, &(message), (TickType_t) 0) == pdPASS) {

			for (size_t i = 0; i < readCallBack.callBaclNum; i++) {
				if (readCallBack.pCallBaclFunc[i]) {
					// call function using pointer
					(readCallBack.pCallBaclFunc[i])(message.ucMessageID,
							message.ucDLC, &message.ucData[0]);

				}

			}
		}
		vTaskDelay(2);
	}
}

// canApi send function
uint8_t CanApiSend(uint16_t ID, uint8_t DLC, uint8_t *data) {
	static CanMeassage_t message;
	//use low level function to send data
	if (sentToCan(ID, DLC, data)) { //cant send via low level will use queuer

		message.ucDLC = DLC;
		message.ucMessageID = ID;
		memcpy(&message.ucData[0], data, message.ucDLC);
		xQueueSend(txQueue, &message, 0);
		return (1);
	}
	return (0);
}
