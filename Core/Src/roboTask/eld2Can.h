/*
 * eld2Can.h
 *
 *  Created on: Dec 23, 2022
 *      Author: andrey
 */

#ifndef CORE_SRC_ROBOTASK_ELD2CAN_H_
#define CORE_SRC_ROBOTASK_ELD2CAN_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "canOpenDevice.h"
#include "eldCanData.h"
/*Node statuses*/
enum NODE_STATE {
	NODE_NOTINIT = 0,
	NODE_INIT_STATE,
	NODE_INITIALIZED,
	NODE_ACTIVE,
	NODE_ERROR,
	MAX_NODE
};
/*control mode enum*/
enum LOOP_MODE_STATE {
	POSITION = 1, SPEED = 3, TORQUE, MAX_ENUM_LOOP
};
#define DRIVE_ENABLE 1
#define DRIVE_DISABLE 0
// device structure definition
typedef struct {
	uint8_t devId;
	uint8_t nodeState;
	uint8_t initState;
	uint8_t controlMode;
	uint8_t controlState;
	uint8_t deviceOnline;
	uint8_t cmdWaitCounter;
	bool commandFlashBit;
	uint8_t cmdValue;
	uint8_t dirValue;
	int32_t refValue;
	bool	syncMaster;
	union PDO1_TX_un PDO1_tx;
	union PDO2_TX_un PDO2_tx;
	union PDO3_TX_un PDO3_tx;
	union PDO4_TX_un PDO4_tx;
	union PDO1_RX_un PDO1_rx;
	union PDO2_RX_un PDO2_rx;
	union PDO3_RX_un PDO3_rx;
	struct ELD2_data_st ELD2_data;
	struct ELD2_settings settings;
	union SDO_un SDO_rx;
	void (*set_loopValue)(void *,int32_t);
	void (*set_refValue)(void *,int32_t);
	void (*set_cmdValue)(void *,int32_t);
	void (*set_dirValue)(void *,int32_t);
} eldDevice_st;

void eldDevInit(uint8_t devId,uint8_t syncMaster, eldDevice_st *dev);
void deviceTimeTick(eldDevice_st *dev);
void deviceRxParse(eldDevice_st *dev, uint32_t canId, uint8_t *data);

#endif /* CORE_SRC_ROBOTASK_ELD2CAN_H_ */
