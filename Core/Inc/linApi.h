/*
 * linApi.h
 *
 *  Created on: Jan 30, 2023
 *      Author: andrey
 */

#ifndef CORE_SRC_LINAPI_H_
#define CORE_SRC_LINAPI_H_

#include "usart.h"
typedef struct {

	uint8_t ID;
	uint8_t size;
	uint8_t data[9];		//max payload on LIN 2.0
	uint8_t crc;
}linMessage_st;
extern linMessage_st linMessage;
uint16_t linReadNotify(linMessage_st  *msg);

uint16_t linWriteNotify(linMessage_st  *msg);

void LinApiInit(void);
void LinApiReadProcess(void);
uint8_t LinApiAddCallBack(void *pCallBackFunc);
uint8_t LinApiSend(linMessage_st  *msg);
//Low level send function
uint8_t linSend(linMessage_st  *msg);

#endif /* CORE_SRC_LINAPI_H_ */
