#ifndef OS_IO_H_INCLUDED
#define OS_IO_H_INCLUDED

/*
    This module deals with the problem of thread-safe access to text output for different threads.

    The macro os_printf() is exported, which wraps printf() access.
*/

#ifndef NULL
#define NULL                            ((void *)0)
#endif


uint8_t init_os_io_engine(void);

#define os_printf(...)


#endif /* OS_IO_H_INCLUDED */
