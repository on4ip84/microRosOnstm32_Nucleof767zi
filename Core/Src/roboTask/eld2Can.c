/*
 * eld2Can.c
 *
 *  Created on: Dec 23, 2022
 *      Author: andrey
 */

#include "eld2Can.h"
/*Init parameters state machine*/
enum INIT_PARAM_STATES {
	MAKE_NMT_OPERATIONAL = 0,
	SET_CONTROL_MODE_6060,
	SET_SPEED_LIMIT_6081,
	SET_ACCEL_LIMIT_6083,
	SET_ACCEL_LIMIT_6084,
	SET_ACCEL_LIMIT_6085,
	SET_HOMEBACK_MODE_6098,
	SET_HOMEBACK_SPEED_6099_1,
	SET_HOMEBACK_SPEED_6099_2,
	SET_HOMEBACK_ACCEL_609A,
	SET_HOMEBACK_OFFSET_607C,
	MAX_ENUM
};
void set_dirValue(void *dev, uint8_t dir) {
	// Cast received message to used type
	eldDevice_st *_dev = (eldDevice_st*) dev;
	_dev->dirValue = dir;

}
void set_cmdValue(void *dev, uint8_t cmd) {
	// Cast received message to used type
	eldDevice_st *_dev = (eldDevice_st*) dev;
	_dev->cmdValue = cmd;
	_dev->commandFlashBit = true;
}
void set_refValue(void *dev, int32_t ref) {
	// Cast received message to used type
	eldDevice_st *_dev = (eldDevice_st*) dev;
	_dev->refValue = ref;
}
void set_controlValue(void *dev, uint32_t controlMode) {
	// Cast received message to used type
	eldDevice_st *_dev = (eldDevice_st*) dev;
	_dev->controlMode = controlMode;
}
void eldDevInit(uint8_t devId, uint8_t syncMaster, eldDevice_st *dev) {
	dev->devId = devId;
	dev->syncMaster = syncMaster;
	dev->nodeState = NODE_NOTINIT;
	dev->initState = MAKE_NMT_OPERATIONAL;
	dev->controlMode = TORQUE;
	dev->deviceOnline = 0;
	// Link internal functions
	dev->set_cmdValue = set_cmdValue;
	dev->set_refValue = set_refValue;
	dev->set_dirValue = set_dirValue;
	dev->set_loopValue = set_controlValue;
}

static void deviceInit(eldDevice_st *dev) {
	switch (dev->initState) {
	case MAKE_NMT_OPERATIONAL:
		/*make NMT command with nodeid*/
		sendNMT(dev->devId, 0x1);
		if (dev->deviceOnline) {
			dev->initState = SET_CONTROL_MODE_6060;
		}
		break;
	case SET_CONTROL_MODE_6060: {
		sendSDO(dev->devId, wr1b, 0x6060, 0, dev->controlMode);
		dev->initState = SET_SPEED_LIMIT_6081;
		break;
	}
	case SET_SPEED_LIMIT_6081: {
		/*Motor max speed in rpm/60 * enc resolution*/
		uint32_t motorMaxSpeed =
				(dev->settings.maxMotorSpeed < MAX_MOTOR_SPEED_PULSE) ?
						dev->settings.maxMotorSpeed : MAX_MOTOR_SPEED_PULSE;
		sendSDO(dev->devId, wr4b, 0x6081, 0, motorMaxSpeed);
		dev->initState = SET_ACCEL_LIMIT_6083;
		break;
	}
	case SET_ACCEL_LIMIT_6083: {
		/*Accel to  max speed in 1 sec
		 Accel value  rpm/60 * enc resolution / sec */
		uint32_t motorMaxAceel =
				(dev->settings.maxAcceleration < MAX_MOTOR_ACCEL) ?
						dev->settings.maxAcceleration : MAX_MOTOR_ACCEL;
		sendSDO(dev->devId, wr4b, 0x6083, 0, motorMaxAceel);
		dev->initState = SET_ACCEL_LIMIT_6084;
		break;
	}
	case SET_ACCEL_LIMIT_6084: {
		uint32_t motorMaxAceel =
				(dev->settings.maxDeacceleration < MAX_MOTOR_ACCEL) ?
						dev->settings.maxAcceleration : MAX_MOTOR_ACCEL;
		sendSDO(dev->devId, wr4b, 0x6084, 0, motorMaxAceel);
		dev->initState = SET_ACCEL_LIMIT_6085;
		break;
	}
	case SET_ACCEL_LIMIT_6085: {
		uint32_t motorMaxAceel =
				(dev->settings.maxAcceleration < MAX_MOTOR_ACCEL) ?
						dev->settings.maxAcceleration : MAX_MOTOR_ACCEL;
		sendSDO(dev->devId, wr4b, 0x6085, 0, motorMaxAceel);
		dev->initState = SET_HOMEBACK_MODE_6098;
		break;
	}
	case SET_HOMEBACK_MODE_6098: {
		sendSDO(dev->devId, wr1b, 0x6098, 0, 0);
		dev->initState = SET_HOMEBACK_ACCEL_609A;
		break;
	}
	case SET_HOMEBACK_ACCEL_609A: {
		sendSDO(dev->devId, wr4b, 0x609A, 0, 100);
		dev->initState = SET_HOMEBACK_SPEED_6099_1;
		break;
	}
	case SET_HOMEBACK_SPEED_6099_1: {
		sendSDO(dev->devId, wr4b, 0x6099, 1, 100);
		dev->initState = SET_HOMEBACK_SPEED_6099_2;
		break;
	}
	case SET_HOMEBACK_SPEED_6099_2: {
		sendSDO(dev->devId, wr4b, 0x6099, 2, 100);
		dev->initState = SET_HOMEBACK_OFFSET_607C;
		break;
	}
	case SET_HOMEBACK_OFFSET_607C: {
		sendSDO(dev->devId, wr4b, 0x607C, 0, 0);
		dev->initState = MAX_ENUM;
		break;
	}
	case MAX_ENUM: {
		dev->nodeState = NODE_INITIALIZED;
		break;
	}
	default:
		break;
	}
}

void makePDO1(eldDevice_st *dev) {
	sendPDO(dev->devId, PDO1_TX, &dev->PDO1_tx.pdo_data[0]);
}
void makePDO2(eldDevice_st *dev) {
	sendPDO(dev->devId, PDO2_TX, &dev->PDO2_tx.pdo_data[0]);
}
void makePDO3(eldDevice_st *dev) {
	sendPDO(dev->devId, PDO3_TX, &dev->PDO3_tx.pdo_data[0]);
}
void makePDO4(eldDevice_st *dev) {
	sendPDO(dev->devId, PDO4_TX, &dev->PDO4_tx.pdo_data[0]);
}

static void deviceCtrl(eldDevice_st *dev) {

	/*control state  enum*/
	enum CONTROL_CMD_STATES {
		DISABLE = 0,
		GOTO_READY,
		CLEAR_ERROR,
		GOTO_ENABLED,
		GOTO_ACTIVE,
		GOTO_COMMAMD,
		MAX_ENUM_CONTROL
	};

	int8_t refSign = (dev->dirValue > 0) ? 1 : -1;
	if (dev->commandFlashBit == false) {
		if (dev->cmdWaitCounter-- < 1) {
			dev->cmdValue = 0;
			dev->refValue = 0;
			;
			dev->cmdWaitCounter = CMDWAITCOUNTER;
		}
	} else {
		dev->cmdWaitCounter = CMDWAITCOUNTER;
	}
	// clear connad bit
	dev->commandFlashBit = false;
	if ((dev->nodeState == NODE_INITIALIZED)
			|| (dev->nodeState == NODE_ACTIVE)) {
		if (dev->cmdValue == 0)
			dev->controlState = DISABLE;

		/* Command word state machine */
		switch (dev->controlState) {
		case DISABLE: {
			if (dev->controlMode == TORQUE) {
				dev->PDO1_tx.PDO1_TX_st.command = 0x6;
				dev->PDO1_tx.PDO1_TX_st.torque = 0;
				dev->PDO1_tx.PDO1_TX_st.mode = dev->controlMode;
				makePDO1(dev);
			}
			if (dev->controlMode == POSITION) {
				dev->PDO2_tx.PDO2_TX_st.command = 0x6;
				dev->PDO2_tx.PDO2_TX_st.position = 0;
				dev->PDO2_tx.PDO2_TX_st.mode = dev->controlMode;
				makePDO2(dev);
			}
			if (dev->controlMode == SPEED) {
				dev->PDO3_tx.PDO3_TX_st.command = 0x6;
				dev->PDO3_tx.PDO3_TX_st.speed = 0;
				dev->PDO3_tx.PDO3_TX_st.mode = dev->controlMode;
				makePDO3(dev);
			}

			dev->controlState = CLEAR_ERROR;
			break;
		}
		case CLEAR_ERROR: {
			dev->PDO1_tx.PDO1_TX_st.command = 0xF0;
			dev->PDO1_tx.PDO1_TX_st.torque = 0x0;
			dev->PDO1_tx.PDO1_TX_st.mode = dev->controlMode;
			makePDO1(dev);

			dev->controlState = GOTO_READY;
			break;
		}
		case GOTO_READY: {

			dev->PDO1_tx.PDO1_TX_st.command = 0x6;
			dev->PDO1_tx.PDO1_TX_st.torque = 0x0;
			dev->PDO1_tx.PDO1_TX_st.mode = dev->controlMode;
			makePDO1(dev);

			dev->controlState = GOTO_ENABLED;
			break;
		}
		case GOTO_ENABLED: {
			dev->PDO1_tx.PDO1_TX_st.command = 0x7;
			dev->PDO1_tx.PDO1_TX_st.torque = 0x0;
			dev->PDO1_tx.PDO1_TX_st.mode = dev->controlMode;
			makePDO1(dev);

			dev->controlState = GOTO_ACTIVE;
			break;
		}
		case GOTO_ACTIVE: {
			if (dev->controlMode == TORQUE) {
				dev->PDO1_tx.PDO1_TX_st.command = 0xF;
				dev->PDO1_tx.PDO1_TX_st.torque = dev->refValue * refSign;
				dev->PDO1_tx.PDO1_TX_st.mode = dev->controlMode;
				makePDO1(dev);
			}
			if (dev->controlMode == POSITION) {
				dev->PDO2_tx.PDO2_TX_st.command = 0x2F;
				dev->PDO2_tx.PDO2_TX_st.position = dev->refValue;
				dev->PDO2_tx.PDO2_TX_st.mode = dev->controlMode;
				makePDO2(dev);
			}
			if (dev->controlMode == SPEED) {
				dev->PDO3_tx.PDO3_TX_st.command = 0xF;
				/*Limit actual ref value to Max in settings*/
				int32_t speedRef = dev->refValue;
				if (speedRef > (int32_t) (dev->settings.maxMotorSpeed))
					speedRef = (int32_t) (dev->settings.maxMotorSpeed);
				if (speedRef < (-(int32_t) (dev->settings.maxMotorSpeed)))
					speedRef = -(int32_t) (dev->settings.maxMotorSpeed);
				speedRef = speedRef * MOTOR_SPEED_SCALE;
				dev->PDO3_tx.PDO3_TX_st.speed = (int32_t) (speedRef * refSign);
				dev->PDO3_tx.PDO3_TX_st.mode = dev->controlMode;
				makePDO3(dev);
			}

			dev->nodeState = NODE_ACTIVE;
			dev->controlState = GOTO_COMMAMD;
			break;
		}
		case GOTO_COMMAMD: {
			if (dev->controlMode == TORQUE) {
				dev->PDO1_tx.PDO1_TX_st.command = 0xF;
				dev->PDO1_tx.PDO1_TX_st.torque = (int16_t) (dev->refValue
						* refSign);
				dev->PDO1_tx.PDO1_TX_st.mode = dev->controlMode;
				makePDO1(dev);
			}
			if (dev->controlMode == POSITION) {
				dev->PDO2_tx.PDO2_TX_st.command = 0x3F;
				dev->PDO2_tx.PDO2_TX_st.position = (int32_t) (dev->refValue
						* MOTOR_POSITION_SCALE);
				dev->PDO2_tx.PDO2_TX_st.mode = dev->controlMode;
				makePDO2(dev);
			}
			if (dev->controlMode == SPEED) {
				dev->PDO3_tx.PDO3_TX_st.command = 0xF;
				/*Limit actual ref value to Max in settings*/
				int32_t speedRef = dev->refValue;
				if (speedRef > (int32_t) (dev->settings.maxMotorSpeed))
					speedRef = (int32_t) (dev->settings.maxMotorSpeed);
				if (speedRef < (-(int32_t) (dev->settings.maxMotorSpeed)))
					speedRef = -(int32_t) (dev->settings.maxMotorSpeed);
				speedRef = speedRef * MOTOR_SPEED_SCALE;
				dev->PDO3_tx.PDO3_TX_st.speed = (int32_t) speedRef * refSign;
				dev->PDO3_tx.PDO3_TX_st.mode = dev->controlMode;
				makePDO3(dev);
				dev->PDO4_tx.PDO4_TX_st.torqueLimitPerCent =
						dev->settings.maxTorquePerCent;
				makePDO4(dev);
			}
			dev->controlState = GOTO_ACTIVE;
			break;
		}
		default:
			break;
		}
	}
	/*Update data variable*/
	dev->ELD2_data.control.control_word = dev->PDO1_tx.PDO1_TX_st.command;
	dev->ELD2_data.status.status_word = dev->PDO1_rx.PDO1_RX_st.status;
	dev->ELD2_data.ref_torque = dev->PDO1_tx.PDO1_TX_st.torque;
	dev->ELD2_data.fb_torque = dev->PDO1_rx.PDO1_RX_st.torque;
	dev->ELD2_data.ref_position = dev->PDO2_tx.PDO2_TX_st.position
			/ MOTOR_POSITION_SCALE;
	dev->ELD2_data.fb_position = dev->PDO2_rx.PDO2_RX_st.position
			/ MOTOR_POSITION_SCALE;
	dev->ELD2_data.ref_speed = dev->PDO3_tx.PDO3_TX_st.speed / MOTOR_SPEED_SCALE
			* refSign;
	dev->ELD2_data.fb_speed = dev->PDO3_rx.PDO3_RX_st.speed / MOTOR_SPEED_SCALE
			* refSign;

	dev->ELD2_data.actual_mode = dev->PDO1_rx.PDO1_RX_st.mode;
	/*send SYNC to update PDO data*/
	sendSYNC(dev->syncMaster);
	/*If drive command == clear fault state clear fault code*/
	if (dev->ELD2_data.control.contro_word_st.flear_fault) {
		dev->ELD2_data.status.status_word = 0;
		dev->ELD2_data.error_code = 0;
	}
	/*If drive in fault state read actual fault code*/
	if (dev->ELD2_data.status.status_word_st.fault) {
		sendSDO(dev->devId, rd, 0x603f, 0, 0);
	}
	/*read absolute torqe limit in percent*/
	sendSDO(dev->devId, rd, 0x6072, 0, 0);

}

void deviceTimeTick(eldDevice_st *dev) {
	//if device NOT INITIALIZED try to make it
	if (dev->nodeState == NODE_NOTINIT) {
		deviceInit(dev);

	} else	// work with device
	{
		deviceCtrl(dev);
	}
}

static void rxSDOParse(eldDevice_st *dev, uint8_t *data) {
	memcpy(&dev->SDO_rx.sdo_data[0], data, 8);
	switch (dev->SDO_rx.SDO_st.index) {
	case 0x603f /* EROOR COde*/:
		/* code */
		dev->ELD2_data.error_code = dev->SDO_rx.SDO_st.data;
		break;
	case 0x6072 /* Absolute torque limit*/:
		/* code */
		dev->ELD2_data.torqueLimitPercent = dev->SDO_rx.SDO_st.data;
		break;
	default:
		break;
	}
}
void deviceRxParse(eldDevice_st *dev, uint32_t canId, uint8_t *data) {
	if (!dev->deviceOnline)
		dev->deviceOnline = 1;	// device online if we can read from

	// parse data
	uint16_t msg_type_id = canId & (~dev->devId);
	switch (msg_type_id) {
	case SDO_R:
		/* code */
		rxSDOParse(dev, data);
		break;
	case PDO1_RX:
		memcpy(&dev->PDO1_rx.pdo_data[0], data, 8);
		/* code */
		break;
	case PDO2_RX:
		memcpy(&dev->PDO2_rx.pdo_data[0], data, 8);
		break;
	case PDO3_RX:
		/* code */
		// DM822_data.status_word.status_word = msg->data[0];
		// uint32_t *p = static_cast <uint32_t*> (&msg->data[2]);
		memcpy(&dev->PDO3_rx.pdo_data[0], data, 8);
		break;
	case PDO4_RX: {
		/* code */
		// DM822_data.status_word.status_word = msg->data[0];
		// DM822_data.fb_speed= *((uint32_t*)&msg->data[2]);
		// memcpy(&DM822_data.fb_speed,&msg->data[2],sizeof(DM822_data.fb_speed));
		break;
	}
	default:
		break;
	}
}

