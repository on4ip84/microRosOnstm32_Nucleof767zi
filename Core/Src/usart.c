/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file    usart.c
 * @brief   This file provides code for the configuration
 *          of the USART instances.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */
#include "linApi.h"
/* USER CODE END 0 */

UART_HandleTypeDef huart3;
UART_HandleTypeDef huart6;
DMA_HandleTypeDef hdma_usart3_tx;
DMA_HandleTypeDef hdma_usart3_rx;

/* USART3 init function */

void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}
/* USART6 init function */

void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 19200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  huart6.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart6.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_LIN_Init(&huart6, UART_LINBREAKDETECTLENGTH_11B) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
  if(uartHandle->Instance==USART3)
  {
  /* USER CODE BEGIN USART3_MspInit 0 */

  /* USER CODE END USART3_MspInit 0 */

  /** Initializes the peripherals clock
  */
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3;
    PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
    {
      Error_Handler();
    }

    /* USART3 clock enable */
    __HAL_RCC_USART3_CLK_ENABLE();

    __HAL_RCC_GPIOD_CLK_ENABLE();
    /**USART3 GPIO Configuration
    PD8     ------> USART3_TX
    PD9     ------> USART3_RX
    */
    GPIO_InitStruct.Pin = STLK_RX_Pin|STLK_TX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    /* USART3 DMA Init */
    /* USART3_TX Init */
    hdma_usart3_tx.Instance = DMA1_Stream3;
    hdma_usart3_tx.Init.Channel = DMA_CHANNEL_4;
    hdma_usart3_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_usart3_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart3_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart3_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart3_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart3_tx.Init.Mode = DMA_NORMAL;
    hdma_usart3_tx.Init.Priority = DMA_PRIORITY_VERY_HIGH;
    hdma_usart3_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&hdma_usart3_tx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(uartHandle,hdmatx,hdma_usart3_tx);

    /* USART3_RX Init */
    hdma_usart3_rx.Instance = DMA1_Stream1;
    hdma_usart3_rx.Init.Channel = DMA_CHANNEL_4;
    hdma_usart3_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_usart3_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart3_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart3_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart3_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart3_rx.Init.Mode = DMA_CIRCULAR;
    hdma_usart3_rx.Init.Priority = DMA_PRIORITY_VERY_HIGH;
    hdma_usart3_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&hdma_usart3_rx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(uartHandle,hdmarx,hdma_usart3_rx);

    /* USART3 interrupt Init */
    HAL_NVIC_SetPriority(USART3_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(USART3_IRQn);
  /* USER CODE BEGIN USART3_MspInit 1 */

  /* USER CODE END USART3_MspInit 1 */
  }
  else if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspInit 0 */

  /* USER CODE END USART6_MspInit 0 */

  /** Initializes the peripherals clock
  */
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART6;
    PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
    {
      Error_Handler();
    }

    /* USART6 clock enable */
    __HAL_RCC_USART6_CLK_ENABLE();

    __HAL_RCC_GPIOG_CLK_ENABLE();
    /**USART6 GPIO Configuration
    PG9     ------> USART6_RX
    PG14     ------> USART6_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_14;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /* USER CODE BEGIN USART6_MspInit 1 */

  /* USER CODE END USART6_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART3)
  {
  /* USER CODE BEGIN USART3_MspDeInit 0 */

  /* USER CODE END USART3_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART3_CLK_DISABLE();

    /**USART3 GPIO Configuration
    PD8     ------> USART3_TX
    PD9     ------> USART3_RX
    */
    HAL_GPIO_DeInit(GPIOD, STLK_RX_Pin|STLK_TX_Pin);

    /* USART3 DMA DeInit */
    HAL_DMA_DeInit(uartHandle->hdmatx);
    HAL_DMA_DeInit(uartHandle->hdmarx);

    /* USART3 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART3_IRQn);
  /* USER CODE BEGIN USART3_MspDeInit 1 */

  /* USER CODE END USART3_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspDeInit 0 */

  /* USER CODE END USART6_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART6_CLK_DISABLE();

    /**USART6 GPIO Configuration
    PG9     ------> USART6_RX
    PG14     ------> USART6_TX
    */
    HAL_GPIO_DeInit(GPIOG, GPIO_PIN_9|GPIO_PIN_14);

    /* USART6 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART6_IRQn);
  /* USER CODE BEGIN USART6_MspDeInit 1 */

  /* USER CODE END USART6_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
typedef enum {
	LIN_RECEIVING_BREAK = 0x01,
	LIN_RECEIVING_SYNC,
	LIN_RECEIVING_ID,
	LIN_RECEIVING_DATA,
	LIN_DATA_RECIVED,
	LIN_DATA_SENT,
} LIN_State;
LIN_State slaveState = LIN_RECEIVING_BREAK;
#define LIN_SYNC_BYTE														0x55
#define LIN_TX_ID                                                         0x3A
#define LIN_RX_ID                                                         0x3B
uint8_t linTxId = LIN_TX_ID;
uint8_t linRxId = LIN_RX_ID;

uint16_t rxByte = 0;
uint16_t linSlaveRxCnt = 0;
uint16_t linSlaveTxCnt = 0;

uint8_t LIN_CalcCheckSum(uint8_t *data, uint8_t len) {
	uint16_t sum = 0;

	for (uint8_t i = 0; i < len; i++) {
		sum += data[i];
	}

	while (sum > 0xFF) {
		sum -= 0xFF;
	}

	sum = 0xFF - sum;

	return (uint8_t) sum;
}

uint8_t LIN_ID_Parity_Calculator(uint8_t data) {
	uint8_t parity_bit_even = 0x00;
	uint8_t parity_bit_odd = 0x00;
	uint8_t result = 0x00;

	parity_bit_even = ((data ^ (data >> 1)) ^ ((data >> 2) ^ (data >> 4)));
	parity_bit_odd = (~(((data >> 1) ^ (data >> 3))
			^ ((data >> 4) ^ (data >> 5))));
	result = (parity_bit_odd << 1) | (parity_bit_even);
	return result;
}
#define LIN_DATA_BYTES_NUM                                                9
uint8_t linMasterData[LIN_DATA_BYTES_NUM];
uint8_t linSlaveData[LIN_DATA_BYTES_NUM];
LIN_FlowProcess() {
	uint32_t isrflags = huart6.Instance->ISR;
	//if (slaveState == LIN_RECEIVING_BREAK) {
	if ((isrflags & USART_ISR_LBDF) != RESET) {
		volatile uint16_t tmp = huart6.Instance->RDR & 0xFF;
		__HAL_UART_CLEAR_FLAG(&huart6, USART_ICR_LBDCF);
		//ATOMIC_CLEAR_BIT(huart6.Instance->ICR, USART_ICR_LBDCF);
		//ATOMIC_CLEAR_BIT(huart6.Instance->ICR, USART_ICR_ORECF);
		slaveState = LIN_RECEIVING_SYNC;
		HAL_UART_Receive_IT(&huart6, &rxByte, 1);
		// .....
	}
	//}
	if ((isrflags & USART_ISR_ORE) != RESET) {
		WRITE_REG(huart6.Instance->ICR, USART_ICR_ORECF);
	}

}
// Must calc parity
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	if (huart == &huart6) {
		uint8_t checkSum = 0;
		switch (slaveState) {
		case LIN_RECEIVING_SYNC:
			if (rxByte == LIN_SYNC_BYTE) {
				slaveState = LIN_RECEIVING_ID;
				HAL_UART_Receive_IT(huart, &rxByte, 1);
			} else {
				slaveState = LIN_RECEIVING_BREAK;
			}
			break;
		case LIN_RECEIVING_ID: {
			uint8_t ID = LIN_ID_Parity_Calculator(rxByte & 0x1f);
			ID = rxByte & 0x3f;
			linMessage.ID = ID;
			switch (linMessage.ID&0x30){
			case 0x00:
				linMessage.size = 2;
				break;
			case 0x20:
				linMessage.size = 4;
				break;
			case 0x30:
				linMessage.size = 8;
				break;
			}

			HAL_UART_Receive_IT(huart, &linMessage.data[0],
					linMessage.size+1 );
			slaveState = LIN_RECEIVING_DATA;
//			if (ID == LIN_RX_ID) {
//				slaveState = LIN_DATA_SENT;
//				for (uint8_t i = 0; i < LIN_DATA_BYTES_NUM - 1; i++) {
//					linSlaveData[i] = 0x30 + i;
//				}
//				linSlaveData[LIN_DATA_BYTES_NUM - 1] = LIN_CalcCheckSum(
//						linSlaveData, LIN_DATA_BYTES_NUM - 1);
//				HAL_UART_Transmit_IT(huart, linSlaveData, LIN_DATA_BYTES_NUM);
//			} else {
//				if (ID == LIN_TX_ID) {
//					slaveState = LIN_RECEIVING_DATA;
//					HAL_UART_Receive_IT(huart, linSlaveData,
//					LIN_DATA_BYTES_NUM);
//				} else {
//					slaveState = LIN_RECEIVING_BREAK;
//				}
//			}
			break;
		}
		case LIN_RECEIVING_DATA:
			checkSum = LIN_CalcCheckSum(&linMessage.data[0], linMessage.size);
			linMessage.crc = checkSum;
			if (linMessage.data[linMessage.size] == checkSum) {
				linSlaveRxCnt++;
				slaveState = LIN_DATA_RECIVED;
				linReadNotify(&linMessage);
			}
			break;
		case LIN_DATA_SENT: {
			linSlaveTxCnt++;
			break;
		}
		default:
			break;
		}
	}
}
uint8_t linSend(linMessage_st  *msg)
{
	static uint8_t syncByte = 0x55;
	HAL_LIN_SendBreak(&huart6);
	HAL_UART_Transmit(&huart6, &syncByte, 1, 10);
	HAL_UART_Transmit(&huart6, &msg->ID, 1, 10);
	msg->data[msg->size] = LIN_CalcCheckSum(&msg->data[0],msg->size);
	HAL_UART_Transmit(&huart6, &msg->data[0], msg->size+1, 10);
	return 1;
}
/* USER CODE END 1 */
