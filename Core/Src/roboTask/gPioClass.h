/*
 * gPioClass.h
 *
 *  Created on: Jan 16, 2023
 *      Author: andrey
 */

#ifndef CORE_SRC_ROBOTASK_GPIOCLASS_H_
#define CORE_SRC_ROBOTASK_GPIOCLASS_H_
#include <stdint.h>
#include <stdbool.h>
#include <stm32f7xx_hal.h>
class gPio
{
public:
	gPio(GPIO_TypeDef *_pinPort,uint16_t _pinNum);
	virtual ~gPio();
	/*base functions*/
	virtual void Set(){this->pinPort->BSRR =  pinNum; };
	virtual void Clear(){pinPort->BSRR = (uint32_t)pinNum << 16;};
	bool readState(){pinState = ((pinPort->IDR & pinNum) != (uint32_t) 0);  return pinState;};

private:
	uint16_t pinNum;
	GPIO_TypeDef *pinPort;
	bool pinState;
};

/******************************************************************************/
class gPioIn: public gPio
{
public:
	gPioIn(GPIO_TypeDef *_pinPort,uint16_t _pinNum):gPio(_pinPort, _pinNum){};
	void Set(){};
	void Clear(){};
private:
};

class gPioOut: public gPio
{
public:
	gPioOut(GPIO_TypeDef *_pinPort,uint16_t _pinNum):gPio(_pinPort, _pinNum){};
	void Toggle(){(readState()==true)?Clear():Set();};
private:
};


#endif /* CORE_SRC_ROBOTASK_GPIOCLASS_H_ */
