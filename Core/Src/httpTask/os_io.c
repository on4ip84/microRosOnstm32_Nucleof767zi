#include "os_io.h"

volatile SemaphoreHandle_t printf_mutex;
volatile SemaphoreHandle_t gets_mutex;
volatile uint8_t string_buffer[OS_IO_STRING_BUFFER_LENGTH];

uint8_t init_os_io_engine(void)
{
    printf_mutex = xSemaphoreCreateMutex();

    if (printf_mutex!=NULL)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

char* eth_gets(volatile circular_buffer_descriptor_t *src,char *str)
{
    uint16_t k;
    uint16_t sz;
    uint8_t byte;

    sz = 0;

    while (sz == 0)
    {
        sz = circular_buffer_get_data_byte_count((circular_buffer_descriptor_t *)src);
        vTaskDelay(100);
    }

    for (k=0; k<sz; k++)
    {
        circular_buffer_extract_byte((circular_buffer_descriptor_t *)src,&byte);
        str[k] = byte;

        if (str[k] == '\n')
        {
            break;
        }
    }

    str[k] = 0;

    return str;
}

uint16_t scan_files_on_SD_card(uint8_t **file_list)
{
    static FF_FindData_t pxFindStruct;
    uint16_t file_count;

    file_count = 0;

    memset(&pxFindStruct,0,sizeof(FF_FindData_t));

    if (ff_findfirst("/sdcard",&pxFindStruct)==0)
    {
        do
        {
            if (!(pxFindStruct.ucAttributes & FF_FAT_ATTR_DIR))
            {
                file_list[file_count] = (uint8_t *) pvPortMalloc(strlen(pxFindStruct.pcFileName) + 1);

                if (file_list[file_count] != NULL)
                {
                    strcpy(file_list[file_count],pxFindStruct.pcFileName);
                    file_count++;
                }

                if (file_count >= MAX_FILES_SCANNED)
                {
                    break;
                }
            }
        }
        while (ff_findnext(&pxFindStruct)==0);
    }

    return file_count;
}

void free_file_list(uint8_t **file_list,uint16_t entries_no)
{
    uint16_t k;

    for (k=0; k<entries_no; k++)
    {
        vPortFree(file_list[k]);
    }
}
