/*
 * canOpenDevice.c
 *
 *  Created on: Dec 23, 2022
 *      Author: andrey
 */
#include "canOpenDevice.h"

// helper data structure
union sdoHelper_un {
	struct sdo_st {
		uint8_t cmd;
		uint16_t index;
		uint8_t subIndex;
		uint32_t data;
	} sdoStruct;
	uint8_t data[8];
} sdoHelper;
uint8_t rawData[8] = { };
/*Send NMT helper function*/
void sendNMT(uint8_t devId, uint8_t cmd) {
	/*make NMT command with nodeid*/

	rawData[0] = cmd; /*NMT Operation command*/
	rawData[1] = devId; /*Node ID*/
	// Send data
	CanApiSend(0, 2, &rawData[0]);
}

// function send PDO structure data to CAN
void sendPDO(uint8_t devId, uint16_t pdoNum, uint8_t *data) {
	CanApiSend(pdoNum + devId, 8, data);
}
/*Send SYNC helper function*/
void sendSYNC(uint8_t syncEna) {
  /*make NMT command with nodeid*/
  /*SYNC broadcast ID 0x80*/
  /*SYNC data 0x0*/
  /*Send SYNC message only if node is a master*/
  if (syncEna)
	  CanApiSend(0x80, 0, &rawData[0]);
}
// function send SDO structure data to CAN
void sendSDO(uint8_t devId, uint8_t cmd, uint16_t index, uint8_t subIndex,
		uint32_t data) {
	//Pack data to sdo array
	sdoHelper.sdoStruct.cmd = cmd;
	sdoHelper.sdoStruct.index = index;
	sdoHelper.sdoStruct.subIndex = subIndex;
	sdoHelper.sdoStruct.data = data;
	// Send data
	CanApiSend(0x600 + devId, 8, &sdoHelper.data[0]);

}

