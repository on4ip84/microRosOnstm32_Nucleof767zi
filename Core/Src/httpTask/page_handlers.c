#include "webserver.h"
#include "base64_encdec.h"

extern TaskHandle_t launcher_task_handle;
extern TaskHandle_t script_task_handle;
void launcher_task(void *params);

parameter_t post_parsed[MAX_PARSED_ENRTIES];

uint16_t file_request_handler(uint8_t *post_request,uint32_t request_length,uint8_t *html_buffer,uint32_t html_buffer_length)
{
	return 1;
}

uint16_t upload_handler(uint8_t *post_request,uint32_t request_length,uint8_t *html_buffer,uint32_t html_buffer_length)
{
	return 1;
}

#define CON_BUF_LENGTH      80
#define CON_ENCODED_LENGTH  ((CON_BUF_LENGTH*4)/3 + 10)

uint16_t console_data_gateway(uint8_t *post_request,uint32_t request_length,uint8_t *html_buffer,uint32_t html_buffer_length)
{
		return 1;
}
const page_handler_t page_handlers[] = {
    {"/file_request_handler",file_request_handler},
    {"/script_loader",upload_handler},
    {"/console_gateway",console_data_gateway},
    {"\0",NULL}
};
