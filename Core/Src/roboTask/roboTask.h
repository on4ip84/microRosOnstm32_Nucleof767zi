/*
 * roboTask.h
 *
 *  Created on: Dec 22, 2022
 *      Author: andrey
 */

#ifndef CORE_SRC_ROBOTASK_ROBOTASK_H_
#define CORE_SRC_ROBOTASK_ROBOTASK_H_


#include "canApi.h"


void roboTaskInit(void);

#endif /* CORE_SRC_ROBOTASK_ROBOTASK_H_ */
